package com.epam.lab.repository;

import com.epam.lab.builder.AuthorBuilder;
import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.model.Author;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContextConfig.class})
@ActiveProfiles("dev")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class AuthorRepositoryImplTest {

    private static final String TABLE_NAME = "authors";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void testSave_ShouldPass() {
        TestTransaction.flagForCommit();
        Author author = new AuthorBuilder()
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        Author savedAuthor = authorRepository.save(author);
        int authorCount = JdbcTestUtils.countRowsInTable(this.jdbcTemplate, TABLE_NAME);
        assertEquals(21, savedAuthor.getId().longValue());
        assertEquals("Name-test01", savedAuthor.getName());
        assertEquals("Surname-test01", savedAuthor.getSurname());
        assertEquals(21, authorCount);
    }

    @Test
    public void testUpdate_AuthorExist_ShouldPass() {
        TestTransaction.flagForCommit();
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Sergei")
                .withSurname("Volosevich")
                .build();
        Author updateAuthor = authorRepository.update(author);
        assertEquals(1, updateAuthor.getId().longValue());
        assertEquals("Sergei", updateAuthor.getName());
        assertEquals("Volosevich", updateAuthor.getSurname());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdate_AuthorNotExist_ShouldThrowEntityNotFoundException() {
        Author author = new AuthorBuilder()
                .withId(500L)
                .withName("Sergei")
                .withSurname("Volosevich")
                .build();
        authorRepository.update(author);
    }

    @Test
    public void testFindById_AuthorExist_ShouldPass() {
        Author foundAuthor = authorRepository.findById(1L);
        assertEquals("Name-test01", foundAuthor.getName());
        assertEquals("Surname-test01", foundAuthor.getSurname());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testFindById_AuthorNotExist_ShouldThrowEntityNotFoundException() {
        authorRepository.findById(500L);
    }

    @Test
    public void testFindAll_ShouldPass() {
        List<Author> authors = authorRepository.findAll();
        assertEquals(20, authors.size());
    }

    @Test
    public void testDeleteById_AuthorExist_ShouldPass() {
        TestTransaction.flagForCommit();
        authorRepository.deleteById(1L);
        TestTransaction.end();
        int authorCount = JdbcTestUtils.countRowsInTable(this.jdbcTemplate, TABLE_NAME);
        assertEquals(19, authorCount);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteById_AuthorNotExist_ShouldThrowEntityNotFoundException() {
        authorRepository.deleteById(500L);
    }
}