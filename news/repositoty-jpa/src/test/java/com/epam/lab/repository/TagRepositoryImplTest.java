package com.epam.lab.repository;

import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.model.Tag;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestContextConfig.class})
@ActiveProfiles("dev")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class TagRepositoryImplTest {

    private static final String TABLE_NAME = "tags";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void testSave_ShouldPass() {
        TestTransaction.flagForCommit();
        Tag tag = new Tag();
        tag.setName("Tag_test01");
        Tag savedTag = tagRepository.save(tag);
        int tagCount = JdbcTestUtils.countRowsInTable(this.jdbcTemplate, TABLE_NAME);
        assertEquals(21, savedTag.getId().longValue());
        assertEquals(21, tagCount);
    }

    @Test
    public void testUpdate_TagExist_ShouldPass() {
        TestTransaction.flagForCommit();
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("Tag_test01");
        Tag updateTag = tagRepository.update(tag);
        assertEquals(1, updateTag.getId().longValue());
        assertEquals("Tag_test01", updateTag.getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdate_TagNotExist_ShouldThrowEntityNotFoundException() {
        Tag tag = new Tag();
        tag.setId(500L);
        tag.setName("Tag_test01");
        tagRepository.update(tag);
    }

    @Test
    public void testFindById_ExistTag_ShouldPass() {
        Tag foundTag = tagRepository.findById(1L);
        assertEquals("tag-test01", foundTag.getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testFindById_NotExistTag_ShouldPass() {
        tagRepository.findById(500L);
    }

    @Test
    public void testFindAll_ShouldPass() {
        List<Tag> tags = tagRepository.findAll();
        assertEquals(20, tags.size());
    }

    @Test
    public void testDeleteById_TagExist_ShouldPass() {
        TestTransaction.flagForCommit();
        tagRepository.deleteById(1L);
        TestTransaction.end();
        int tagCount = JdbcTestUtils.countRowsInTable(this.jdbcTemplate, TABLE_NAME);
        assertEquals(19, tagCount);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteById_TagNotExist_ShouldPass() {
        tagRepository.deleteById(500L);
    }
}