package com.epam.lab.repository;

import com.epam.lab.builder.AuthorBuilder;
import com.epam.lab.builder.NewsBuilder;
import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.exception.SearchParameterNotFoundException;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContextConfig.class})
@ActiveProfiles("dev")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class NewsRepositoryImplTest {

    private static final String TABLE_NAME = "news";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Autowired
    private NewsRepository newsRepository;

    @Test
    public void testSave_ShouldPass() {
        TestTransaction.flagForCommit();
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("tag-test01");
        News news = new NewsBuilder()
                .withTitle("Title-test01")
                .withShortText("Short text in test01")
                .withFullText("Some text of news in test01")
                .withCreationDate(LocalDate.parse("2020-01-28"))
                .withModificationDate(LocalDate.parse("2020-01-28"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(tag1)
                .build();

        News savedNews = newsRepository.save(news);
        int newsCount = JdbcTestUtils.countRowsInTable(this.jdbcTemplate, TABLE_NAME);

        assertEquals(27, savedNews.getId().longValue());
        assertEquals("Title-test01", savedNews.getTitle());
        assertEquals("Short text in test01", savedNews.getShortText());
        assertEquals("Some text of news in test01", savedNews.getFullText());
        assertEquals(LocalDate.parse("2020-01-28"), savedNews.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-28"), savedNews.getModificationDate());
        Author author = savedNews.getAuthor();
        assertEquals(1, author.getId().longValue());
        assertEquals("Name-test01", author.getName());
        assertEquals("Surname-test01", author.getSurname());
        Set<Tag> tags = savedNews.getTags();
        assertEquals(1, tags.size());
        assertEquals(27, newsCount);
    }

    @Test
    public void testUpdate_NewsExist_ShouldPass() {
        TestTransaction.flagForCommit();
        Tag tag1 = new Tag();
        tag1.setId(5L);
        tag1.setName("tag-test05");
        Tag tag2 = new Tag();
        tag2.setId(4L);
        tag2.setName("tag-test04");
        Tag tag3 = new Tag();
        tag3.setId(7L);
        tag3.setName("tag-test07");
        News news = new NewsBuilder()
                .withId(1L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-28"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(new AuthorBuilder()
                        .withId(10L)
                        .withName("Name-test10")
                        .withSurname("Surname-test10")
                        .build())
                .withTag(tag1)
                .withTag(tag2)
                .withTag(tag3)
                .build();
        News updateNews = newsRepository.update(news);
        Author author = updateNews.getAuthor();
        Set<Tag> tags = updateNews.getTags();
        assertEquals(1, updateNews.getId().longValue());
        assertEquals("Title", updateNews.getTitle());
        assertEquals("Short text", updateNews.getShortText());
        assertEquals("Some text of news", updateNews.getFullText());
        assertEquals(LocalDate.parse("2020-01-28"), updateNews.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-30"), updateNews.getModificationDate());
        assertEquals(10, author.getId().longValue());
        assertEquals("Name-test10", author.getName());
        assertEquals("Surname-test10", author.getSurname());
        assertEquals(3, tags.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdate_NewsNotExist_ShouldThrowEntityNotFoundException() {
        News news = new NewsBuilder()
                .withId(500L)
                .build();
        newsRepository.update(news);
    }

    @Test
    public void testFindById_NewsExist_ShouldPass() {
        News foundNews = newsRepository.findById(1L);
        Set<Tag> tags = foundNews.getTags();
        Author author = foundNews.getAuthor();
        assertEquals("Title-test01", foundNews.getTitle());
        assertEquals("Short text in test01", foundNews.getShortText());
        assertEquals("Some text of news in test01", foundNews.getFullText());
        assertEquals(LocalDate.parse("2020-01-28"), foundNews.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-28"), foundNews.getModificationDate());
        assertEquals(1, author.getId().longValue());
        assertEquals("Name-test01", author.getName());
        assertEquals("Surname-test01", author.getSurname());
        assertEquals(2, tags.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testFindById_NewsNotExist_ShouldThrowEntityNotFoundException() {
        newsRepository.findById(500L);
    }

    @Test
    public void testFindAll_ShouldPass() {
        List<News> news = newsRepository.findAll();
        assertEquals(26, news.size());
    }

    @Test
    public void testDeleteById_NewsExist_ShouldPass() {
        TestTransaction.flagForCommit();
        newsRepository.deleteById(5L);
        TestTransaction.end();
        int newsCount = JdbcTestUtils.countRowsInTable(this.jdbcTemplate, TABLE_NAME);
        assertEquals(25, newsCount);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteById_NewsNotExist_ShouldThrowEntityNotFoundException() {
        newsRepository.deleteById(500L);
    }

    @Test
    public void testFindAll_ByNewsTitle_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("title", ":", "Title-test01");
        NewsSearchSpecification newsSearchSpecification = new NewsSearchSpecification(searchCriteria);
        List<SortCriteria> sortCriteria = new ArrayList<>();
        NewsSortSpecification sortSpecification = new NewsSortSpecification(sortCriteria);
        List<News> foundNews = newsRepository.findAll(newsSearchSpecification, sortSpecification);
        assertEquals(1, foundNews.size());
        News news = foundNews.get(0);
        Author author = news.getAuthor();
        Set<Tag> tags = news.getTags();
        assertEquals(1, news.getId().longValue());
        assertEquals("Title-test01", news.getTitle());
        assertEquals("Short text in test01", news.getShortText());
        assertEquals("Some text of news in test01", news.getFullText());
        assertEquals(LocalDate.parse("2020-01-28"), news.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-28"), news.getModificationDate());
        assertEquals(1, author.getId().longValue());
        assertEquals("Name-test01", author.getName());
        assertEquals("Surname-test01", author.getSurname());
        assertEquals(2, tags.size());
    }

    @Test
    public void testFindAll_ByNewsTitleLikeTitle_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("title", ":", "Title");
        NewsSearchSpecification newsSearchSpecification = new NewsSearchSpecification(searchCriteria);
        List<SortCriteria> sortCriteria = new ArrayList<>();
        NewsSortSpecification sortSpecification = new NewsSortSpecification(sortCriteria);
        List<News> foundNews = newsRepository.findAll(newsSearchSpecification, sortSpecification);
        assertEquals(26, foundNews.size());
    }

    @Test
    public void testFindAll_ByAuthorNameAuthorSurname_ShouldPass() {
        SearchCriteria authorNameCriteria =
                new SearchCriteria("author_name", ":", "Name-test01");
        NewsSearchSpecification authorNameSpecification =
                new NewsSearchSpecification(authorNameCriteria);
        SearchCriteria authorSurnameCriteria =
                new SearchCriteria("author_surname", ":", "Surname-test01");
        NewsSearchSpecification authorSurnameSpecification =
                new NewsSearchSpecification(authorSurnameCriteria);
        Specification<News> commonSpecification = authorNameSpecification.and(authorSurnameSpecification);
        List<SortCriteria> sortCriteria = new ArrayList<>();
        sortCriteria.add(new SortCriteria("author_name", false));
        sortCriteria.add(new SortCriteria("creationDate", false));
        NewsSortSpecification sortSpecification = new NewsSortSpecification(sortCriteria);
        List<News> foundNews = newsRepository.findAll(commonSpecification, sortSpecification);
        assertEquals(2, foundNews.size());
        News news1 = foundNews.get(0);
        Author author1 = news1.getAuthor();
        Set<Tag> tags1 = news1.getTags();
        assertEquals(1, news1.getId().longValue());
        assertEquals("Title-test01", news1.getTitle());
        assertEquals("Short text in test01", news1.getShortText());
        assertEquals("Some text of news in test01", news1.getFullText());
        assertEquals(LocalDate.parse("2020-01-28"), news1.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-28"), news1.getModificationDate());
        assertEquals(1, author1.getId().longValue());
        assertEquals("Name-test01", author1.getName());
        assertEquals("Surname-test01", author1.getSurname());
        assertEquals(2, tags1.size());
        News news2 = foundNews.get(1);
        Author author2 = news2.getAuthor();
        Set<Tag> tags2 = news2.getTags();
        assertEquals(2, news2.getId().longValue());
        assertEquals("Title-test02", news2.getTitle());
        assertEquals("Short text in test02", news2.getShortText());
        assertEquals("Some text of news in test02", news2.getFullText());
        assertEquals(LocalDate.parse("2020-01-27"), news2.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-29"), news2.getModificationDate());
        assertEquals(1, author2.getId().longValue());
        assertEquals("Name-test01", author2.getName());
        assertEquals("Surname-test01", author2.getSurname());
        assertEquals(1, tags2.size());
    }

    @Test(expected = IncorrectResultSizeDataAccessException.class)
    public void testFindOne_ByAuthorNameAuthorSurname_ShouldPass() {
        SearchCriteria authorNameCriteria =
                new SearchCriteria("author_name", ":", "Name-test01");
        NewsSearchSpecification authorNameSpecification =
                new NewsSearchSpecification(authorNameCriteria);
        SearchCriteria authorSurnameCriteria =
                new SearchCriteria("author_surname", ":", "Surname-test01");
        NewsSearchSpecification authorSurnameSpecification =
                new NewsSearchSpecification(authorSurnameCriteria);
        Specification<News> commonSpecification = authorNameSpecification.and(authorSurnameSpecification);
        Optional<News> newsOptional = newsRepository.findOne(commonSpecification);
        assertTrue(newsOptional.isPresent());
    }

    @Test
    public void testFindOne_ByAuthorName_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("author_name", ":", "Name-test15");
        NewsSearchSpecification authorNameSpecification =
                new NewsSearchSpecification(searchCriteria);
        Optional<News> newsOptional = newsRepository.findOne(authorNameSpecification);
        assertTrue(newsOptional.isPresent());
        News news = newsOptional.get();
        Author author = news.getAuthor();
        Set<Tag> tags = news.getTags();
        assertEquals(16, news.getId().longValue());
        assertEquals("Title-test16", news.getTitle());
        assertEquals("Short text in test16", news.getShortText());
        assertEquals("Some text of news in test16", news.getFullText());
        assertEquals(LocalDate.parse("2020-01-25"), news.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-31"), news.getModificationDate());
        assertEquals(15, author.getId().longValue());
        assertEquals("Name-test15", author.getName());
        assertEquals("Surname-test15", author.getSurname());
        assertEquals(1, tags.size());
    }

    @Test
    public void testFindOne_ByAuthorNameNotExist_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("author_name", ":", "Name-test500");
        NewsSearchSpecification authorNameSpecification =
                new NewsSearchSpecification(searchCriteria);
        Optional<News> newsOptional = newsRepository.findOne(authorNameSpecification);
        assertFalse(newsOptional.isPresent());
    }

    @Test
    public void testFindAll_ByModificationDateGreaterThan_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("modificationDate", ">", "2020-01-30");
        NewsSearchSpecification modificationDateSpecification =
                new NewsSearchSpecification(searchCriteria);
        List<SortCriteria> sortCriteria = new ArrayList<>();
        sortCriteria.add(new SortCriteria("creationDate", false));
        sortCriteria.add(new SortCriteria("author_name", true));
        NewsSortSpecification sortSpecification = new NewsSortSpecification(sortCriteria);
        List<News> news = newsRepository.findAll(modificationDateSpecification, sortSpecification);
        assertEquals(7, news.size());
    }

    @Test
    public void testFindAll_ByModificationDateLessThan_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("modificationDate", "<", "2020-01-30");
        NewsSearchSpecification modificationDateSpecification =
                new NewsSearchSpecification(searchCriteria);
        List<SortCriteria> sortCriteria = new ArrayList<>();
        sortCriteria.add(new SortCriteria("creationDate", false));
        sortCriteria.add(new SortCriteria("author_surname", true));
        NewsSortSpecification sortSpecification = new NewsSortSpecification(sortCriteria);
        List<News> news = newsRepository.findAll(modificationDateSpecification, sortSpecification);
        assertEquals(24, news.size());
    }

    @Test
    public void testFindAll_ByModificationDate_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("modificationDate", ":", "2020-01-30");
        NewsSearchSpecification modificationDateSpecification =
                new NewsSearchSpecification(searchCriteria);
        List<SortCriteria> sortCriteria = new ArrayList<>();
        NewsSortSpecification sortSpecification = new NewsSortSpecification(sortCriteria);
        List<News> news = newsRepository.findAll(modificationDateSpecification, sortSpecification);
        assertEquals(5, news.size());
    }

    @Test(expected = SearchParameterNotFoundException.class)
    public void testFindAllShouldTrowSearchParameterNotFoundException() {
        SearchCriteria searchCriteria = new SearchCriteria("creation_Date_Local", ":", "2020-01-30");
        NewsSearchSpecification searchSpecification = new NewsSearchSpecification(searchCriteria);
        NewsSortSpecification sortSpecification = new NewsSortSpecification(new ArrayList<>());
        newsRepository.findAll(searchSpecification, sortSpecification);
    }

    @Test
    public void testFindAll_ByTagName_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("tags_name", ":", "tag_test01");
        NewsSearchSpecification searchSpecification = new NewsSearchSpecification(searchCriteria);
        NewsSortSpecification sortSpecification = new NewsSortSpecification(new ArrayList<>());
        List<News> news = newsRepository.findAll(searchSpecification, sortSpecification);
        assertEquals(3, news.size());
    }

    @Test
    public void testFindAll_ByCreationDate_SortModificationDateDesc_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("creationDate", ":", "2020-01-28");
        NewsSearchSpecification searchSpecification = new NewsSearchSpecification(searchCriteria);
        List<SortCriteria> sortCriteria = new ArrayList<>();
        sortCriteria.add(new SortCriteria("modificationDate", false));
        NewsSortSpecification sortSpecification = new NewsSortSpecification(sortCriteria);
        List<News> news = newsRepository.findAll(searchSpecification, sortSpecification);
        assertEquals(11, news.size());
        assertEquals(LocalDate.parse("2020-01-30"), news.get(0).getModificationDate());
        assertEquals(LocalDate.parse("2020-01-28"), news.get(10).getCreationDate());
    }

    @Test(expected = SearchParameterNotFoundException.class)
    public void testFindAll_ByAuthorName() {
        SearchCriteria searchCriteria = new SearchCriteria("author_name", "<", "Name-test01");
        NewsSearchSpecification searchSpecification = new NewsSearchSpecification(searchCriteria);
        NewsSortSpecification sortSpecification = new NewsSortSpecification(new ArrayList<>());
        newsRepository.findAll(searchSpecification, sortSpecification);
    }

    @Test
    public void testCountNews_ShouldPass() {
        long count = newsRepository.countNews();
        assertEquals(26, count);
    }

    @Test
    public void testFindAll_WithLimit_ShouldPass() {
        List<News> news = newsRepository.findAll(0, 10);
        assertEquals(10, news.size());
        assertEquals(1L, news.get(0).getId().longValue());
        assertEquals(10L, news.get(9).getId().longValue());
    }

    @Test
    public void testCountFoundNews_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("tags_name", ":", "tag_test01");
        NewsSearchSpecification searchSpecification = new NewsSearchSpecification(searchCriteria);
        long totalCount = newsRepository.countFoundEntities(searchSpecification);
        assertEquals(3, totalCount);
    }

    @Test
    public void testFindAllInParts_WithoutSorting_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("tags_name", ":", "tag");
        NewsSearchSpecification searchSpecification = new NewsSearchSpecification(searchCriteria);
        NewsSortSpecification sortSpecification = new NewsSortSpecification(new ArrayList<>());
        List<News> news = newsRepository.findAll(searchSpecification, sortSpecification, 0, 5);
        assertEquals(5, news.size());
        assertEquals(LocalDate.of(2020, 1, 30), news.get(0).getCreationDate());
        assertEquals(LocalDate.of(2020, 1, 28), news.get(4).getCreationDate());
        assertEquals(26, news.get(0).getId().longValue());
        assertEquals(18, news.get(4).getId().longValue());
    }

    @Test
    public void testFindAllInParts_WithSorting_ShouldPass() {
        SearchCriteria searchCriteria = new SearchCriteria("tags_name", ":", "tag");
        NewsSearchSpecification searchSpecification = new NewsSearchSpecification(searchCriteria);
        List<SortCriteria> sortCriteria = new ArrayList<>();
        sortCriteria.add(new SortCriteria("modificationDate", false));
        NewsSortSpecification sortSpecification = new NewsSortSpecification(sortCriteria);
        List<News> news = newsRepository.findAll(searchSpecification, sortSpecification, 0, 5);
        assertEquals(5, news.size());
        assertEquals(LocalDate.of(2020, 1, 31), news.get(0).getModificationDate());
        assertEquals(LocalDate.of(2020, 1, 30), news.get(4).getModificationDate());
        assertEquals(16, news.get(0).getId().longValue());
        assertEquals(6, news.get(4).getId().longValue());
        news.forEach(item -> System.out.println(item.getCreationDate()));
    }
}