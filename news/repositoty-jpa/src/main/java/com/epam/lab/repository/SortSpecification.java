package com.epam.lab.repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.List;

public interface SortSpecification<T> {
    List<Order> toOrder(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder);
}
