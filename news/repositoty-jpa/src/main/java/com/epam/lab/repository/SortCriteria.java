package com.epam.lab.repository;

import java.util.Objects;

public class SortCriteria {
    private String key;
    private boolean isAscending;

    public SortCriteria() {
    }

    public SortCriteria(String key, boolean isAscending) {
        this.key = key;
        this.isAscending = isAscending;
    }

    public String getKey() {
        return key;
    }

    public boolean isAscending() {
        return isAscending;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SortCriteria that = (SortCriteria) o;
        return isAscending == that.isAscending &&
                Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, isAscending);
    }
}
