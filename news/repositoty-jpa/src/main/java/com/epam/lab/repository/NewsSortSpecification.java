package com.epam.lab.repository;

import com.epam.lab.model.News;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

public class NewsSortSpecification implements SortSpecification<News> {

    private static final String CREATION_DATE_FIELD = "creationDate";

    private final List<SortCriteria> sortCriteria;

    public NewsSortSpecification(List<SortCriteria> sortCriteria) {
        this.sortCriteria = sortCriteria;
    }

    @Override
    public List<Order> toOrder(Root<News> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        return sortCriteria.stream().map(sc -> {
            String key = sc.getKey();
            String[] keys = key.split("_");
            if (keys.length == 1) {
                return getOrder(builder, sc, root.get(sc.getKey()));
            } else if (keys.length == 2) {
                String keyName = keys[0];
                String keyValue = keys[1];
                return getOrder(builder, sc, root.get(keyName).get(keyValue));
            }
            return builder.desc(root.get(CREATION_DATE_FIELD));
        }).collect(Collectors.toList());
    }

    private Order getOrder(CriteriaBuilder builder, SortCriteria sc, Path<Object> objectPath) {
        if (sc.isAscending()) {
            return builder.asc(objectPath);
        }
        return builder.desc(objectPath);
    }
}
