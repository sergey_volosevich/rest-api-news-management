package com.epam.lab.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySources(value = {@PropertySource("classpath:database.properties"), @PropertySource("classpath:"
        + "persistence.properties")})
@ComponentScan("com.epam.lab.repository")
public class PersistenceContextConfig {

    private static final String URL = "url";
    private static final String DRIVER = "driver";
    private static final String USER = "dbuser";
    private static final String DB_PASS = "dbpassword";
    private static final String POOL_SIZE = "size";
    private static final String TEST_DB_SQL = "test-db.sql";
    private static final String TEST_DATA_SQL = "test-data.sql";
    private static final String MODEL_LOCATION = "com.epam.lab.model";
    private static final String HIBERNATE_DIALECT = "hibernate.dialect";
    private static final String PRODUCTION_PROFILE = "prod";
    private static final String DEVELOPMENT_PROFILE = "dev";
    private static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private static final String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    private static final String HIBERNATE_USE_SQL_COMMENTS = "hibernate.use_sql_comments";
    private static final String CURRENT_SESSION_CONTEXT_CLASS = "hibernate.current_session_context_class";
    private static final int DEFAULT_POOL_SIZE = 10;

    private final Environment environment;

    @Autowired
    public PersistenceContextConfig(Environment environment) {
        this.environment = environment;
    }

    @Bean
    @Profile(PRODUCTION_PROFILE)
    public DataSource hikariDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(environment.getProperty(DRIVER));
        hikariConfig.setJdbcUrl(environment.getProperty(URL));
        hikariConfig.setUsername(environment.getProperty(USER));
        hikariConfig.setPassword(environment.getProperty(DB_PASS));
        hikariConfig.setMaximumPoolSize(environment.getProperty(POOL_SIZE, Integer.class, DEFAULT_POOL_SIZE));
        return new HikariDataSource(hikariConfig);
    }


    @Bean
    @Profile(DEVELOPMENT_PROFILE)
    public DataSource embeddedDataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        return builder.setType(EmbeddedDatabaseType.HSQL).addScript(TEST_DB_SQL).addScript(TEST_DATA_SQL).build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(MODEL_LOCATION);
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        HibernateJpaDialect jpaDialect = new HibernateJpaDialect();
        em.setJpaDialect(jpaDialect);
        em.setJpaProperties(jpaProperties());
        return em;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(entityManagerFactory);
        return jpaTransactionManager;
    }

    @Bean
    public BeanPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    private Properties jpaProperties() {
        Properties properties = new Properties();
        properties.setProperty(HIBERNATE_DIALECT, environment.getProperty(HIBERNATE_DIALECT));
        properties.setProperty(HIBERNATE_SHOW_SQL, environment.getProperty(HIBERNATE_SHOW_SQL));
        properties.setProperty(CURRENT_SESSION_CONTEXT_CLASS, environment.getProperty(CURRENT_SESSION_CONTEXT_CLASS));
        properties.setProperty(HIBERNATE_FORMAT_SQL, environment.getProperty(HIBERNATE_FORMAT_SQL));
        properties.setProperty(HIBERNATE_USE_SQL_COMMENTS, environment.getProperty(HIBERNATE_USE_SQL_COMMENTS));
        return properties;
    }
}
