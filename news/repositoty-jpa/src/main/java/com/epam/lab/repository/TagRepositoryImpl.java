package com.epam.lab.repository;

import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private static final Logger LOGGER = LogManager.getLogger(TagRepositoryImpl.class);

    private static final String FAILED_TO_FIND_BY_ID = "Failed to find tag by id - {}. Tag does not exist.";
    private static final String ENTITY_NOT_FOUND_MSG = "Tag with id - {0} does not exist";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Tag save(Tag entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public Tag update(Tag entity) {
        long id = entity.getId();
        findById(id);
        return entityManager.merge(entity);
    }

    @Override
    public Tag findById(Long id) {
        Tag foundTag = entityManager.find(Tag.class, id);
        if (Objects.isNull(foundTag)) {
            LOGGER.error(FAILED_TO_FIND_BY_ID, id);
            throw new EntityNotFoundException(MessageFormat.format(ENTITY_NOT_FOUND_MSG, id));
        }
        return foundTag;
    }

    @Override
    public List<Tag> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> criteriaQuery = criteriaBuilder.createQuery(Tag.class);
        Root<Tag> root = criteriaQuery.from(Tag.class);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public void deleteById(Long id) {
        Tag foundTag = findById(id);
        Set<News> news = foundTag.getNews();
        news.forEach(entity -> entity.setTags(null));
        entityManager.remove(foundTag);
    }
}
