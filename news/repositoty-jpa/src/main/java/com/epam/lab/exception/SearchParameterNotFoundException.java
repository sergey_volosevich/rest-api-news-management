package com.epam.lab.exception;

public class SearchParameterNotFoundException extends RepositoryException {

    private static final long serialVersionUID = 8421277238774282082L;

    public SearchParameterNotFoundException(String message) {
        super(message);
    }
}
