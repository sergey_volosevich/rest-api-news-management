package com.epam.lab.repository;

import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Repository
public class NewsRepositoryImpl implements NewsRepository {

    private static final Logger LOGGER = LogManager.getLogger(NewsRepositoryImpl.class);
    private static final String FAILED_TO_FIND_BY_ID = "Failed to find news by id - {}. News does not exist.";
    private static final String ENTITY_NOT_FOUND_MSG = "News with id - {0} does not exist";
    private static final String FAILED_TO_FIND_NEWS_CAUSE = "Failed to find news. Cause:";
    private static final String CREATION_DATE_FIELD = "creationDate";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public News save(News entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public News update(News entity) {
        long id = entity.getId();
        findById(id);
        return entityManager.merge(entity);
    }

    @Override
    public News findById(Long id) {
        News foundNews = entityManager.find(News.class, id);
        if (Objects.isNull(foundNews)) {
            LOGGER.error(FAILED_TO_FIND_BY_ID, id);
            throw new EntityNotFoundException(MessageFormat.format(ENTITY_NOT_FOUND_MSG, id));
        }
        return foundNews;
    }

    @Override
    public List<News> findAll() {
        CriteriaQuery<News> criteriaQuery = getNewsCriteriaQuery();
        Root<News> root = criteriaQuery.from(News.class);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    private CriteriaQuery<News> getNewsCriteriaQuery() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        return criteriaBuilder.createQuery(News.class);
    }

    @Override
    public void deleteById(Long id) {
        News foundNews = findById(id);
        Set<Tag> tags = foundNews.getTags();
        for (Tag tag : tags) {
            tag.getNews().remove(foundNews);
        }
        entityManager.remove(foundNews);
    }

    @Override
    public List<News> findAll(Specification<News> spec, SortSpecification<News> sortSpec) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> query = builder.createQuery(News.class);
        Root<News> root = query.from(News.class);
        Predicate predicate = spec.toPredicate(root, query, builder);
        query.where(predicate);
        query.select(root);
        List<Order> orders = sortSpec.toOrder(root, query, builder);
        if (orders.isEmpty()) {
            query.orderBy(builder.desc(root.get(CREATION_DATE_FIELD)));
            query.distinct(true);
            return entityManager.createQuery(query).getResultList();
        }
        query.orderBy(orders);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public Optional<News> findOne(Specification<News> spec) {
        try {
            CriteriaBuilder builder = entityManager.getCriteriaBuilder();
            CriteriaQuery<News> query = builder.createQuery(News.class);
            Root<News> root = query.from(News.class);
            Predicate predicate = spec.toPredicate(root, query, builder);
            query.where(predicate);
            query.select(root);
            News foundNews = entityManager.createQuery(query).getSingleResult();
            return Optional.of(foundNews);
        } catch (NoResultException e) {
            LOGGER.error(FAILED_TO_FIND_NEWS_CAUSE, e);
            return Optional.empty();
        }
    }

    @Override
    public List<News> findAll(Specification<News> spec, SortSpecification<News> sortSpec, int limit, int offset) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> query = cb.createQuery(News.class);
        Root<News> root = query.from(News.class);
        Predicate predicate = spec.toPredicate(root, query, cb);
        query.where(predicate);
        query.select(root);
        List<Order> orders = sortSpec.toOrder(root, query, cb);
        if (orders.isEmpty()) {
            query.orderBy(cb.desc(root.get(CREATION_DATE_FIELD)));
            query.distinct(true);
            TypedQuery<News> typedQuery = getNewsTypedQuery(limit, offset, query);
            return typedQuery.getResultList();
        }
        query.orderBy(orders);
        TypedQuery<News> typedQuery = getNewsTypedQuery(limit, offset, query);
        return typedQuery.getResultList();
    }

    private TypedQuery<News> getNewsTypedQuery(int limit, int offset, CriteriaQuery<News> query) {
        TypedQuery<News> typedQuery = entityManager.createQuery(query);
        typedQuery.setFirstResult(limit);
        typedQuery.setMaxResults(offset);
        return typedQuery;
    }

    @Override
    public long countFoundEntities(Specification<News> spec) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<News> root = query.from(News.class);
        query.select(builder.countDistinct(root));
        Predicate predicate = spec.toPredicate(root, query, builder);
        query.where(predicate);
        return entityManager.createQuery(query).getSingleResult();
    }

    @Override
    public List<News> findAll(int limit, int offset) {
        CriteriaQuery<News> criteriaQuery = getNewsCriteriaQuery();
        Root<News> root = criteriaQuery.from(News.class);
        criteriaQuery.select(root);
        TypedQuery<News> query = getNewsTypedQuery(limit, offset, criteriaQuery);
        return query.getResultList();
    }

    @Override
    public long countNews() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        query.select(builder.count(query.from(News.class)));
        return entityManager.createQuery(query).getSingleResult();
    }
}
