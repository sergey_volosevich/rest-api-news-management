package com.epam.lab.exception;

public class EntityNotFoundException extends RepositoryException {

    private static final long serialVersionUID = 4243959991945834489L;

    public EntityNotFoundException() {
    }

    public EntityNotFoundException(String message) {
        super(message);
    }
}
