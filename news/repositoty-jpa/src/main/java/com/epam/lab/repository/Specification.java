package com.epam.lab.repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;

import static com.epam.lab.repository.SpecificationComposition.composed;

public interface Specification<T> extends Serializable {

    default Specification<T> and(Specification<T> other) {
        return composed(this, other, CriteriaBuilder::and);
    }

    Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder);

}
