package com.epam.lab.repository;

import com.epam.lab.exception.SearchParameterNotFoundException;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.MessageFormat;
import java.time.LocalDate;

public class NewsSearchSpecification implements Specification<News> {

    private static final long serialVersionUID = -4786125174986747077L;

    private static final String FAILED_TO_FIND_SEARCH_FOR_FIELD = "Failed to find search for field - {0}";
    private static final String FAILED_TO_FIND_SEARCH_WITH_OPERATION = "Failed to find search for field {0}"
            + " with operation {1}";
    private static final String LIKE_PATTERN = "%{0}%";

    private final SearchCriteria criteria;

    public NewsSearchSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(Root<News> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        String key = criteria.getKey();
        String[] keys = key.split("_");
        String value = criteria.getValue();
        String operation = criteria.getOperation();
        if (keys.length == 1) {
            if (operation.equalsIgnoreCase(">")) {
                return getPredicateOperationGreaterThan(root, criteriaBuilder, key, value);
            } else if (operation.equalsIgnoreCase("<")) {
                return getPredicateOperationLessThan(root, criteriaBuilder, key, value);
            } else if (operation.equalsIgnoreCase(":")) {
                if (root.get(key).getJavaType() == String.class) {
                    return criteriaBuilder.like(root.get(key), MessageFormat.format(LIKE_PATTERN, value));
                } else if (root.get(key).getJavaType() == LocalDate.class) {
                    return criteriaBuilder.equal(root.get(key), LocalDate.parse(value));
                }
            }
        } else if (keys.length == 2) {
            String keyName = keys[0];
            String keyValue = keys[1];
            if (operation.equalsIgnoreCase(":")) {
                return getPredicateForCompositeKey(root, criteriaBuilder, value, keyName, keyValue);
            } else if (operation.equalsIgnoreCase("<") || operation.equalsIgnoreCase(">")) {
                throw new SearchParameterNotFoundException(MessageFormat.format(FAILED_TO_FIND_SEARCH_WITH_OPERATION,
                        key, operation));
            }
        }
        throw new SearchParameterNotFoundException(MessageFormat.format(FAILED_TO_FIND_SEARCH_FOR_FIELD, key));
    }

    /*
    This method is used to get predicate from composite key. For example: key is author_name.
     */

    private Predicate getPredicateForCompositeKey(Root<News> root, CriteriaBuilder criteriaBuilder,
                                                  String value, String keyName, String keyValue) {
        if (root.get(keyName).getJavaType() == Author.class) {
            return criteriaBuilder.like(root.get(keyName).get(keyValue), MessageFormat.format(LIKE_PATTERN, value));
        } else {
            return criteriaBuilder.like(root.join(keyName).get(keyValue), MessageFormat.format(LIKE_PATTERN, value));
        }
    }

    /*
    This method is used to get predicate if the operation value is "<".
     */

    private Predicate getPredicateOperationLessThan(Root<News> root, CriteriaBuilder criteriaBuilder,
                                                    String key, String value) {
        if (root.get(key).getJavaType() == LocalDate.class) {
            return criteriaBuilder.lessThanOrEqualTo(root.get(key), LocalDate.parse(value));
        }
        return criteriaBuilder.lessThanOrEqualTo(root.get(key), value);
    }

    /*
    This method is used to get predicate if the operation value is ">".
     */

    private Predicate getPredicateOperationGreaterThan(Root<News> root, CriteriaBuilder criteriaBuilder,
                                                       String key, String value) {
        if (root.get(key).getJavaType() == LocalDate.class) {
            return criteriaBuilder.greaterThanOrEqualTo(root.get(key), LocalDate.parse(value));
        }
        return criteriaBuilder.greaterThanOrEqualTo(root.get(key), value);
    }
}
