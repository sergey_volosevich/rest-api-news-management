package com.epam.lab.repository;

import com.epam.lab.model.News;

import java.util.List;
import java.util.Optional;

public interface JpaSpecificationExecutor<T> {

    List<T> findAll(Specification<T> spec, SortSpecification<T> sortSpec);

    List<News> findAll(int limit, int offset);

    Optional<T> findOne(Specification<T> spec);

    List<T> findAll(Specification<T> spec, SortSpecification<T> sortSpec, int limit, int offset);

    long countFoundEntities(Specification<T> spec);
}
