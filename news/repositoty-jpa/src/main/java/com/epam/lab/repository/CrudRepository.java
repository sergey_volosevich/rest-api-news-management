package com.epam.lab.repository;

import java.util.List;

public interface CrudRepository<T, I> {
    T save(T entity);

    T update(T entity);

    T findById(I id);

    List<T> findAll();

    void deleteById(I id);
}
