package com.epam.lab.exception;

public class RepositoryException extends RuntimeException {

    private static final long serialVersionUID = -2709238801386216720L;

    public RepositoryException() {
    }

    public RepositoryException(String message) {
        super(message);
    }
}
