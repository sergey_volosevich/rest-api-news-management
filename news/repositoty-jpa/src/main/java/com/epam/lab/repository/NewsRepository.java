package com.epam.lab.repository;

import com.epam.lab.model.News;

public interface NewsRepository extends CrudRepository<News, Long>, JpaSpecificationExecutor<News> {

    long countNews();
}
