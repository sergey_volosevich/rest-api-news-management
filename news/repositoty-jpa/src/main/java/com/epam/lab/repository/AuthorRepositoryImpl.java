package com.epam.lab.repository;

import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.model.Author;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;


@Repository
public class AuthorRepositoryImpl implements AuthorRepository {

    private static final Logger LOGGER = LogManager.getLogger(AuthorRepositoryImpl.class);

    private static final String FAILED_TO_FIND_BY_ID = "Failed to find author by id - {}. Author does not exist.";
    private static final String ENTITY_NOT_FOUND_MSG = "Author with id - {0} does not exist";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Author save(Author entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public Author update(Author entity) {
        long id = entity.getId();
        findById(id);
        return entityManager.merge(entity);
    }

    @Override
    public Author findById(Long id) {
        Author foundAuthor = entityManager.find(Author.class, id);
        if (Objects.isNull(foundAuthor)) {
            LOGGER.error(FAILED_TO_FIND_BY_ID, id);
            throw new EntityNotFoundException(MessageFormat.format(ENTITY_NOT_FOUND_MSG, id));
        }
        return foundAuthor;
    }

    @Override
    public List<Author> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Author> criteriaQuery = criteriaBuilder.createQuery(Author.class);
        Root<Author> root = criteriaQuery.from(Author.class);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public void deleteById(Long id) {
        Author foundAuthor = findById(id);
        entityManager.remove(foundAuthor);
    }
}
