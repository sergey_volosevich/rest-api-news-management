package com.epam.lab.dto;

import java.io.Serializable;
import java.util.Objects;

public class Pagination implements Serializable {

    private static final long serialVersionUID = 5275832669506212180L;

    private int limit;
    private int page;

    public Pagination() {
    }

    public Pagination(int limit, int page) {
        this.limit = limit;
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pagination that = (Pagination) o;
        return limit == that.limit &&
                page == that.page;
    }

    @Override
    public int hashCode() {
        return Objects.hash(limit, page);
    }
}
