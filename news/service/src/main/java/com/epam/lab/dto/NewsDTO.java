package com.epam.lab.dto;

import com.epam.lab.transfer.NewsTransfer;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.PastOrPresent;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

public class NewsDTO implements Serializable {

    private static final long serialVersionUID = -2607411927709281968L;

    @Null(message = "Id field must be absent", groups = {NewsTransfer.New.class})
    @NotNull(message = "Please provide a correct value for id", groups = {NewsTransfer.UpdateNews.class})
    @Min(value = 1, message = "Please provide a correct value for id", groups = {NewsTransfer.UpdateNews.class})
    private Long id;
    @NotBlank(message = "Please provide a title of news", groups = {NewsTransfer.New.class,
            NewsTransfer.UpdateNews.class})
    @Length(min = 10, max = 30, message = "Length of title of news must be between 3 - 30 characters",
            groups = {NewsTransfer.New.class, NewsTransfer.UpdateNews.class})
    private String title;
    @NotBlank(message = "Please provide a short text of news", groups = {NewsTransfer.New.class,
            NewsTransfer.UpdateNews.class})
    @Length(min = 20, max = 100, message = "Length of short text of news must be between 10 - 100 characters",
            groups = {NewsTransfer.New.class, NewsTransfer.UpdateNews.class})
    private String shortText;
    @NotBlank(message = "Please provide a full text of news", groups = {NewsTransfer.New.class,
            NewsTransfer.UpdateNews.class})
    @Length(min = 50, max = 2000, message = "Length of full text of news must be between 50 - 2000 characters",
            groups = {NewsTransfer.New.class, NewsTransfer.UpdateNews.class})
    private String fullText;
    @PastOrPresent(message = "Please provide a correct creation date of news",
            groups = {NewsTransfer.New.class, NewsTransfer.UpdateNews.class})
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @NotNull(message = "Please provide a creation date of news", groups = {NewsTransfer.New.class,
            NewsTransfer.UpdateNews.class})
    private LocalDate creationDate;
    @FutureOrPresent(message = "Please provide a correct modification date of news", groups = {NewsTransfer.New.class,
            NewsTransfer.UpdateNews.class})
    @NotNull(message = "Please provide a modification date of news", groups = {NewsTransfer.New.class,
            NewsTransfer.UpdateNews.class})
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate modificationDate;
    @NotNull(message = "Please provide author of news", groups = {NewsTransfer.New.class,
            NewsTransfer.UpdateNews.class})
    @Valid
    private AuthorDTO author;
    @NotEmpty(message = "Please provide some tags for news", groups = {NewsTransfer.New.class,
            NewsTransfer.UpdateNews.class})
    private Set<@Valid TagDTO> tags;

    public NewsDTO() {
    }

    public NewsDTO(Long id, String title, String shortText, String fullText, LocalDate creationDate,
                   LocalDate modificationDate, AuthorDTO author, Set<TagDTO> tags) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.author = author;
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public AuthorDTO getAuthor() {
        return author;
    }

    public void setAuthor(AuthorDTO author) {
        this.author = author;
    }

    public Set<TagDTO> getTags() {
        return tags;
    }

    public void setTags(Set<TagDTO> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewsDTO newsDTO = (NewsDTO) o;
        return Objects.equals(id, newsDTO.id) &&
                Objects.equals(title, newsDTO.title) &&
                Objects.equals(shortText, newsDTO.shortText) &&
                Objects.equals(fullText, newsDTO.fullText) &&
                Objects.equals(creationDate, newsDTO.creationDate) &&
                Objects.equals(modificationDate, newsDTO.modificationDate) &&
                Objects.equals(author, newsDTO.author) &&
                Objects.equals(tags, newsDTO.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, shortText, fullText, creationDate, modificationDate, author, tags);
    }
}
