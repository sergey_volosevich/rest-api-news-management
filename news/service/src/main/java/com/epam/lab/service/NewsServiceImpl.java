package com.epam.lab.service;

import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.Page;
import com.epam.lab.dto.Pagination;
import com.epam.lab.dto.SearchCriteriaDTO;
import com.epam.lab.dto.SortCriteriaDTO;
import com.epam.lab.exception.ResourceNotFoundException;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.AuthorRepository;
import com.epam.lab.repository.NewsRepository;
import com.epam.lab.repository.NewsSortSpecification;
import com.epam.lab.repository.SearchCriteria;
import com.epam.lab.repository.SortCriteria;
import com.epam.lab.repository.Specification;
import com.epam.lab.repository.TagRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {

    private static final Logger LOGGER = LogManager.getLogger(NewsServiceImpl.class);

    private static final String CAN_NOT_FIND_RESOURCE = "Can not find resource on page {0}. Page does not exist.";
    private static final String FAILED_TO_GET_NEWS = "Failed to get news with pagination";
    private static final String PAGINATION_PARAMETER_TEMPLATE = "limit:{0};page:{1};";

    private final NewsRepository newsRepository;
    private final AuthorRepository authorRepository;
    private final TagRepository tagRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public NewsServiceImpl(NewsRepository newsRepository, AuthorRepository authorRepository,
                           TagRepository tagRepository, ModelMapper modelMapper) {
        this.newsRepository = newsRepository;
        this.authorRepository = authorRepository;
        this.tagRepository = tagRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public NewsDTO save(NewsDTO newsDTO) {
        News news = modelMapper.map(newsDTO, News.class);
        Author author = news.getAuthor();
        Set<Tag> tagsToNews = news.getTags();
        if (Objects.isNull(author.getId())) {
            authorRepository.save(author);
        }
        authorRepository.findById(author.getId());
        tagsToNews.forEach(tag -> {
            if (Objects.isNull(tag.getId())) {
                tagRepository.save(tag);
            }
        });
        tagsToNews.forEach(tag -> tagRepository.findById(tag.getId()));
        News savedNews = newsRepository.save(news);
        return modelMapper.map(savedNews, NewsDTO.class);
    }

    @Override
    public NewsDTO update(NewsDTO newsDTO) {
        News newsToUpdate = modelMapper.map(newsDTO, News.class);
        Author authorForUpdateNews = newsToUpdate.getAuthor();
        Set<Tag> tagsForUpdateNews = newsToUpdate.getTags();
        if (Objects.isNull(authorForUpdateNews.getId())) {
            authorRepository.save(authorForUpdateNews);
        }
        authorRepository.findById(authorForUpdateNews.getId());
        tagsForUpdateNews.forEach(tag -> {
            if (Objects.isNull(tag.getId())) {
                tagRepository.save(tag);
            }
        });
        tagsForUpdateNews.forEach(tag -> tagRepository.findById(tag.getId()));
        News updatedNews = newsRepository.update(newsToUpdate);
        return modelMapper.map(updatedNews, NewsDTO.class);
    }

    @Override
    public void delete(long id) {
        newsRepository.deleteById(id);
    }

    @Override
    public NewsDTO getById(long id) {
        News foundNews = newsRepository.findById(id);
        return modelMapper.map(foundNews, NewsDTO.class);
    }

    @Override
    public List<NewsDTO> getAll() {
        List<News> news = newsRepository.findAll();
        return news
                .stream()
                .map(foundNews -> modelMapper.map(foundNews, NewsDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<NewsDTO> findByCriteria(List<SearchCriteriaDTO> searchCriteria, List<SortCriteriaDTO> sortCriteria) {
        Specification<News> newsSearchSpecification = getNewsSpecification(searchCriteria);
        NewsSortSpecification sortSpecification = getNewsSortSpecification(sortCriteria);
        List<News> news = newsRepository.findAll(newsSearchSpecification, sortSpecification);
        return news.stream().map(foundNews -> modelMapper.map(foundNews, NewsDTO.class)).collect(Collectors.toList());
    }

    private Specification<News> getNewsSpecification(List<SearchCriteriaDTO> searchCriteria) {
        List<SearchCriteria> criteriaToSearchNews = searchCriteria.stream()
                .map(searchCriteriaDTO -> modelMapper.map(searchCriteriaDTO, SearchCriteria.class))
                .collect(Collectors.toList());
        NewsSpecificationBuilder builder = new NewsSpecificationBuilder();
        criteriaToSearchNews.forEach(criteria -> builder.with(criteria.getKey(), criteria.getOperation(),
                criteria.getValue()));
        return builder.build();
    }

    @Override
    public Page<NewsDTO> getNewsInParts(Pagination pagination) {
        long numberOfRows = newsRepository.countNews();
        if (numberOfRows == 0) {
            return getEmptyPage();
        }
        long numberOfPages = getNumberOfPages(pagination, numberOfRows);
        int start = pagination.getPage() * pagination.getLimit() - pagination.getLimit();
        List<News> news = newsRepository.findAll(start, pagination.getLimit());
        return getPageOfNews(pagination, (int) numberOfRows, numberOfPages, news);
    }

    private Page<NewsDTO> getEmptyPage() {
        Page<NewsDTO> page = new Page<>();
        page.setTotalCount(0);
        page.setEntities(new ArrayList<>());
        return page;
    }

    @Override
    public Page<NewsDTO> findByCriteria(List<SearchCriteriaDTO> searchCriteria,
                                        List<SortCriteriaDTO> sortCriteria, Pagination pagination) {
        Specification<News> newsSearchSpecification = getNewsSpecification(searchCriteria);
        NewsSortSpecification sortSpecification = getNewsSortSpecification(sortCriteria);
        long numberOfRows = newsRepository.countFoundEntities(newsSearchSpecification);
        if (numberOfRows == 0) {
            return getEmptyPage();
        }
        long numberOfPages = getNumberOfPages(pagination, numberOfRows);
        int start = pagination.getPage() * pagination.getLimit() - pagination.getLimit();
        List<News> news = newsRepository.findAll(newsSearchSpecification, sortSpecification, start,
                pagination.getLimit());
        return getPageOfNews(pagination, (int) numberOfRows, numberOfPages, news);
    }

    private Page<NewsDTO> getPageOfNews(Pagination pagination, int numberOfRows, long numberOfPages, List<News> news) {
        List<NewsDTO> foundNews = news.stream()
                .map(entity -> modelMapper.map(entity, NewsDTO.class))
                .collect(Collectors.toList());
        Page<NewsDTO> page = new Page<>();
        int nextPage = pagination.getPage() + 1;
        int previousPage = pagination.getPage() - 1;
        int lastPage = (int) numberOfPages;
        if (previousPage != 0) {
            page.setPreviousPage(MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, pagination.getLimit(),
                    previousPage));
        }
        if (nextPage <= numberOfPages) {
            page.setNextPage(MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, pagination.getLimit(), nextPage));
        }
        page.setLastPage(MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, pagination.getLimit(), lastPage));
        page.setEntities(foundNews);
        page.setTotalCount(numberOfRows);
        return page;
    }

    private NewsSortSpecification getNewsSortSpecification(List<SortCriteriaDTO> sortCriteria) {
        List<SortCriteria> criteriaToSortNews = sortCriteria.stream()
                .map(sortCriteriaDTO -> modelMapper.map(sortCriteriaDTO, SortCriteria.class))
                .collect(Collectors.toList());
        return new NewsSortSpecification(criteriaToSortNews);
    }

    private long getNumberOfPages(Pagination pagination, long numberOfRows) {
        long numberOfPages = numberOfRows / pagination.getLimit();
        if (numberOfRows % pagination.getLimit() > 0) {
            numberOfPages++;
        }
        if (pagination.getPage() > numberOfPages) {
            LOGGER.error(FAILED_TO_GET_NEWS);
            String msg = MessageFormat.format(CAN_NOT_FIND_RESOURCE, pagination.getPage());
            throw new ResourceNotFoundException(msg);
        }
        return numberOfPages;
    }

    @Override
    public long countNews() {
        return newsRepository.countNews();
    }
}
