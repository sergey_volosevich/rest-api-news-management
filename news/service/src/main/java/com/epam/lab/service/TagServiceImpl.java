package com.epam.lab.service;

import com.epam.lab.dto.TagDTO;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.TagRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, ModelMapper modelMapper) {
        this.tagRepository = tagRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public TagDTO save(TagDTO tagDTO) {
        Tag tag = convertToEntity(tagDTO);
        Tag savedTag = tagRepository.save(tag);
        return convertToDTO(savedTag);
    }

    @Override
    public TagDTO update(TagDTO tagDTO) {
        Tag tag = convertToEntity(tagDTO);
        Tag updatedTag = tagRepository.update(tag);
        return convertToDTO(updatedTag);
    }

    @Override
    public void delete(long id) {
        tagRepository.deleteById(id);
    }

    @Override
    public TagDTO getById(long id) {
        Tag foundTag = tagRepository.findById(id);
        return convertToDTO(foundTag);
    }

    @Override
    public List<TagDTO> getAll() {
        List<Tag> tags = tagRepository.findAll();
        return tags.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    private TagDTO convertToDTO(Tag tag) {
        return modelMapper.map(tag, TagDTO.class);
    }

    private Tag convertToEntity(TagDTO tagDTO) {
        return modelMapper.map(tagDTO, Tag.class);
    }
}
