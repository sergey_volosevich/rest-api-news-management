package com.epam.lab.dto;

import java.io.Serializable;
import java.util.Objects;

public class SortCriteriaDTO implements Serializable {

    private static final long serialVersionUID = 5099850433395204983L;

    private String key;
    private boolean isAscending;

    public SortCriteriaDTO() {
    }

    public SortCriteriaDTO(String key, boolean isAscending) {
        this.key = key;
        this.isAscending = isAscending;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isAscending() {
        return isAscending;
    }

    public void setAscending(boolean ascending) {
        isAscending = ascending;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SortCriteriaDTO that = (SortCriteriaDTO) o;
        return isAscending == that.isAscending &&
                Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, isAscending);
    }
}
