package com.epam.lab.service;

import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.Page;
import com.epam.lab.dto.Pagination;
import com.epam.lab.dto.SearchCriteriaDTO;
import com.epam.lab.dto.SortCriteriaDTO;

import java.util.List;

public interface NewsService extends Service<NewsDTO> {

    List<NewsDTO> findByCriteria(List<SearchCriteriaDTO> searchCriteria, List<SortCriteriaDTO> sortCriteria);

    Page<NewsDTO> getNewsInParts(Pagination pagination);

    Page<NewsDTO> findByCriteria(List<SearchCriteriaDTO> searchCriteria,
                                 List<SortCriteriaDTO> sortCriteria, Pagination pagination);

    long countNews();
}
