package com.epam.lab.dto;

import com.epam.lab.transfer.NewsTransfer;
import com.epam.lab.transfer.TagTransfer;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Objects;

public class TagDTO implements Serializable {

    private static final long serialVersionUID = -7014311413478051510L;

    @Null(message = "Id field must be absent", groups = {TagTransfer.New.class})
    @NotNull(message = "Please provide a correct value for id", groups = {TagTransfer.UpdateTag.class})
    @Min(value = 1, message = "Please provide a correct value for id", groups = {TagTransfer.UpdateTag.class})
    private Long id;
    @NotBlank(message = "Please provide a tag", groups = {TagTransfer.UpdateTag.class,
            TagTransfer.New.class, NewsTransfer.New.class, NewsTransfer.UpdateNews.class})
    @Pattern(regexp = "^[а-яА-ЯёЁa-zA-Z0-9_]{3,27}$", message = "Please provide a correct value for tag",
            groups = {TagTransfer.UpdateTag.class, TagTransfer.New.class, NewsTransfer.New.class,
                    NewsTransfer.UpdateNews.class})
    private String name;

    public TagDTO() {
    }

    public TagDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagDTO tagDTO = (TagDTO) o;
        return Objects.equals(id, tagDTO.id) &&
                Objects.equals(name, tagDTO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
