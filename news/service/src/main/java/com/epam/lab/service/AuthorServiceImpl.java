package com.epam.lab.service;

import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.model.Author;
import com.epam.lab.repository.AuthorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository, ModelMapper modelMapper) {
        this.authorRepository = authorRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public AuthorDTO save(AuthorDTO authorDTO) {
        Author author = convertToEntity(authorDTO);
        Author savedAuthor = authorRepository.save(author);
        return convertToDTO(savedAuthor);
    }


    @Override
    public AuthorDTO update(AuthorDTO authorDTO) {
        Author author = convertToEntity(authorDTO);
        Author updatedAuthor = authorRepository.update(author);
        return convertToDTO(updatedAuthor);
    }

    @Override
    public void delete(long id) {
        authorRepository.deleteById(id);
    }

    @Override
    public AuthorDTO getById(long id) {
        Author foundAuthor = authorRepository.findById(id);
        return convertToDTO(foundAuthor);
    }

    @Override
    public List<AuthorDTO> getAll() {
        List<Author> authors = authorRepository.findAll();
        return authors.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    private AuthorDTO convertToDTO(Author author) {
        return modelMapper.map(author, AuthorDTO.class);
    }

    private Author convertToEntity(AuthorDTO authorDTO) {
        return modelMapper.map(authorDTO, Author.class);
    }
}
