package com.epam.lab.service;

import com.epam.lab.model.News;
import com.epam.lab.repository.NewsSearchSpecification;
import com.epam.lab.repository.SearchCriteria;
import com.epam.lab.repository.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NewsSpecificationBuilder implements SpecificationBuilder<News> {

    private final List<SearchCriteria> params;

    public NewsSpecificationBuilder() {
        this.params = new ArrayList<>();
    }

    public NewsSpecificationBuilder with(String key, String operation, String value) {
        params.add(new SearchCriteria(key, operation, value));
        return this;
    }

    @Override
    public Specification<News> build() {
        List<NewsSearchSpecification> specifications = params.stream()
                .map(NewsSearchSpecification::new)
                .collect(Collectors.toList());
        Specification<News> result = null;
        for (int i = 0; i < specifications.size(); i++) {
            Specification<News> newsSpecification = specifications.get(i);
            if (i == 0) {
                result = newsSpecification;
            } else {
                result = result.and(newsSpecification);
            }
        }
        return result;
    }
}
