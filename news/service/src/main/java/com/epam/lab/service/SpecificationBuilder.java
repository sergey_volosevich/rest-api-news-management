package com.epam.lab.service;

import com.epam.lab.repository.Specification;

public interface SpecificationBuilder<T> {
    Specification<T> build();
}
