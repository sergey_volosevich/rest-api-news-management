package com.epam.lab.dto;

import java.io.Serializable;
import java.util.Objects;

public class SearchCriteriaDTO implements Serializable {

    private static final long serialVersionUID = 2240656744980477941L;

    private String key;
    private String operation;
    private String value;

    public SearchCriteriaDTO() {
    }

    public SearchCriteriaDTO(String key, String operation, String value) {
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchCriteriaDTO that = (SearchCriteriaDTO) o;
        return Objects.equals(key, that.key) &&
                Objects.equals(operation, that.operation) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, operation, value);
    }
}
