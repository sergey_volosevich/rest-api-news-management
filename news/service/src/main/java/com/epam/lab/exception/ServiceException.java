package com.epam.lab.exception;

public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = -5633456985304517578L;

    public ServiceException(String message) {
        super(message);
    }
}
