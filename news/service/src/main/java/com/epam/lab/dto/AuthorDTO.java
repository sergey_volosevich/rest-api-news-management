package com.epam.lab.dto;

import com.epam.lab.transfer.AuthorTransfer;
import com.epam.lab.transfer.NewsTransfer;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Objects;

public class AuthorDTO implements Serializable {

    private static final long serialVersionUID = 5963282983127664960L;

    @Null(message = "Id field must be absent", groups = {AuthorTransfer.New.class})
    @NotNull(message = "Please provide a correct value for id", groups = {AuthorTransfer.UpdateAuthor.class})
    @Min(value = 1, message = "Please provide a correct value for id",
            groups = {AuthorTransfer.UpdateAuthor.class})
    private Long id;
    @NotBlank(message = "Please provide a author name", groups = {AuthorTransfer.New.class,
            AuthorTransfer.UpdateAuthor.class, NewsTransfer.New.class, NewsTransfer.UpdateNews.class})
    @Pattern(regexp = "^[а-яА-ЯёЁa-zA-Z-]{3,30}$", message = "Please provide a correct value for author name.",
            groups = {AuthorTransfer.New.class, AuthorTransfer.UpdateAuthor.class, NewsTransfer.New.class,
                    NewsTransfer.UpdateNews.class})
    private String name;
    @NotBlank(message = "Please provide a author surname", groups = {AuthorTransfer.New.class,
            AuthorTransfer.UpdateAuthor.class, NewsTransfer.New.class, NewsTransfer.UpdateNews.class})
    @Pattern(regexp = "^[а-яА-ЯёЁa-zA-Z-]{3,30}$", message = "Please provide a correct value for author surname.",
            groups = {AuthorTransfer.New.class, AuthorTransfer.UpdateAuthor.class, NewsTransfer.New.class,
                    NewsTransfer.UpdateNews.class})
    private String surname;

    public AuthorDTO() {
    }

    public AuthorDTO(Long id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorDTO authorDTO = (AuthorDTO) o;
        return Objects.equals(id, authorDTO.id) &&
                Objects.equals(name, authorDTO.name) &&
                Objects.equals(surname, authorDTO.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname);
    }
}
