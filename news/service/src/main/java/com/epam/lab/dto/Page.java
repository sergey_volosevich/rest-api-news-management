package com.epam.lab.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Page<T> implements Serializable {

    private static final long serialVersionUID = 5698950544584635160L;

    private String nextPage;
    private String previousPage;
    private String lastPage;
    private int totalCount;
    private List<T> entities;

    public Page() {
    }

    public Page(String nextPage, String previousPage, String lastPage, int totalCount, List<T> entities) {
        this.nextPage = nextPage;
        this.previousPage = previousPage;
        this.lastPage = lastPage;
        this.totalCount = totalCount;
        this.entities = entities;
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }

    public String getPreviousPage() {
        return previousPage;
    }

    public void setPreviousPage(String previousPage) {
        this.previousPage = previousPage;
    }

    public String getLastPage() {
        return lastPage;
    }

    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<T> getEntities() {
        return entities;
    }

    public void setEntities(List<T> entities) {
        this.entities = entities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Page<?> page = (Page<?>) o;
        return totalCount == page.totalCount &&
                Objects.equals(nextPage, page.nextPage) &&
                Objects.equals(previousPage, page.previousPage) &&
                Objects.equals(lastPage, page.lastPage) &&
                Objects.equals(entities, page.entities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nextPage, previousPage, lastPage, totalCount, entities);
    }
}
