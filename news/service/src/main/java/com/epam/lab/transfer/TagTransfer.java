package com.epam.lab.transfer;

/**
 * This class is needed as indicator, and help to choose {@link javax.validation.ConstraintValidator} to use
 */

public class TagTransfer {
    public interface New {
    }

    public interface Exist {
    }

    public interface UpdateTag extends Exist {
    }

}
