INSERT INTO authors
VALUES (1, 'Name-test01', 'Surname-test01'),
       (2, 'Name-test02', 'Surname-test02'),
       (3, 'Name-test03', 'Surname-test03'),
       (4, 'Name-test04', 'Surname-test04'),
       (5, 'Name-test05', 'Surname-test05'),
       (6, 'Name-test06', 'Surname-test06'),
       (7, 'Name-test07', 'Surname-test07'),
       (8, 'Name-test08', 'Surname-test08'),
       (9, 'Name-test09', 'Surname-test09'),
       (10, 'Name-test10', 'Surname-test10'),
       (11, 'Name-test11', 'Surname-test11'),
       (12, 'Name-test12', 'Surname-test12'),
       (13, 'Name-test13', 'Surname-test13'),
       (14, 'Name-test14', 'Surname-test14'),
       (15, 'Name-test15', 'Surname-test15'),
       (16, 'Name-test16', 'Surname-test16'),
       (17, 'Name-test17', 'Surname-test17'),
       (18, 'Name-test18', 'Surname-test18'),
       (19, 'Name-test19', 'Surname-test19'),
       (20, 'Name-test20', 'Surname-test20');


INSERT INTO news
VALUES (1, 'Title-test01', 'Short text in test01', 'Some text of news in test01', '2020-01-28', '2020-01-28'),
       (2, 'Title-test02', 'Short text in test02', 'Some text of news in test02', '2020-01-27', '2020-01-29'),
       (3, 'Title-test03', 'Short text in test03', 'Some text of news in test03', '2020-01-29', '2020-01-30'),
       (4, 'Title-test04', 'Short text in test04', 'Some text of news in test04', '2020-01-27', '2020-01-31'),
       (5, 'Title-test05', 'Short text in test05', 'Some text of news in test05', '2020-01-26', '2020-01-29'),
       (6, 'Title-test06', 'Short text in test06', 'Some text of news in test06', '2020-01-28', '2020-01-30'),
       (7, 'Title-test07', 'Short text in test07', 'Some text of news in test07', '2020-01-28', '2020-01-29'),
       (8, 'Title-test08', 'Short text in test08', 'Some text of news in test08', '2020-01-28', '2020-01-28'),
       (9, 'Title-test09', 'Short text in test09', 'Some text of news in test09', '2020-01-28', '2020-01-28'),
       (10, 'Title-test10', 'Short text in test10', 'Some text of news in test10', '2020-01-23', '2020-01-28'),
       (11, 'Title-test11', 'Short text in test11', 'Some text of news in test11', '2020-01-24', '2020-01-28'),
       (12, 'Title-test12', 'Short text in test12', 'Some text of news in test12', '2020-01-25', '2020-01-28'),
       (13, 'Title-test13', 'Short text in test13', 'Some text of news in test13', '2020-01-27', '2020-01-28'),
       (14, 'Title-test14', 'Short text in test14', 'Some text of news in test14', '2020-01-28', '2020-01-30'),
       (15, 'Title-test15', 'Short text in test15', 'Some text of news in test15', '2020-01-28', '2020-01-30'),
       (16, 'Title-test16', 'Short text in test16', 'Some text of news in test16', '2020-01-25', '2020-01-31'),
       (17, 'Title-test17', 'Short text in test17', 'Some text of news in test17', '2020-01-21', '2020-01-28'),
       (18, 'Title-test18', 'Short text in test18', 'Some text of news in test18', '2020-01-28', '2020-01-28'),
       (19, 'Title-test19', 'Short text in test19', 'Some text of news in test19', '2020-01-23', '2020-01-28'),
       (20, 'Title-test20', 'Short text in test20', 'Some text of news in test20', '2020-01-25', '2020-01-29'),
       (21, 'Title-test21', 'Short text in test21', 'Some text of news in test21', '2020-01-28', '2020-01-28'),
       (22, 'Title-test22', 'Short text in test22', 'Some text of news in test22', '2020-01-23', '2020-01-27'),
       (23, 'Title-test23', 'Short text in test23', 'Some text of news in test23', '2020-01-28', '2020-01-28'),
       (24, 'Title-test24', 'Short text in test24', 'Some text of news in test24', '2020-01-27', '2020-01-28'),
       (25, 'Title-test25', 'Short text in test25', 'Some text of news in test25', '2020-01-28', '2020-01-28'),
       (26, 'Title-test26', 'Short text in test26', 'Some text of news in test26', '2020-01-30', '2020-01-30');

INSERT INTO tags
VALUES (1, 'tag-test01'),
       (2, 'tag-test02'),
       (3, 'tag-test03'),
       (4, 'tag-test04'),
       (5, 'tag-test05'),
       (6, 'tag-test06'),
       (7, 'tag-test07'),
       (8, 'tag-test08'),
       (9, 'tag-test09'),
       (10, 'tag-test10'),
       (11, 'tag-test11'),
       (12, 'tag-test12'),
       (13, 'tag-test13'),
       (14, 'tag-test14'),
       (15, 'tag-test15'),
       (16, 'tag-test16'),
       (17, 'tag-test17'),
       (18, 'tag-test18'),
       (19, 'tag-test19'),
       (20, 'tag-test20');

INSERT INTO news_author
VALUES (1, 1),
       (2, 1),
       (3, 2),
       (4, 2),
       (5, 4),
       (6, 5),
       (7, 6),
       (8, 7),
       (9, 8),
       (10, 9),
       (11, 10),
       (12, 11),
       (13, 12),
       (14, 13),
       (15, 14),
       (16, 15),
       (17, 16),
       (18, 17),
       (19, 18),
       (20, 19),
       (21, 20),
       (22, 20),
       (23, 17),
       (24, 19),
       (25, 5),
       (26, 6);

INSERT INTO news_tag
VALUES (1, 1),
       (2, 1),
       (3, 2),
       (4, 2),
       (5, 3),
       (6, 3),
       (7, 4),
       (8, 5),
       (9, 6),
       (10, 7),
       (11, 8),
       (12, 9),
       (13, 10),
       (14, 11),
       (15, 12),
       (16, 13),
       (17, 14),
       (18, 15),
       (19, 16),
       (20, 17),
       (21, 18),
       (22, 19),
       (23, 20),
       (24, 5),
       (25, 5),
       (26, 7),
       (3, 1),
       (1, 2);
