package com.epam.lab.service;

import com.epam.lab.builder.AuthorBuilder;
import com.epam.lab.builder.NewsBuilder;
import com.epam.lab.configuration.PersistenceContextConfig;
import com.epam.lab.configuration.ServiceContextConfig;
import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.SearchCriteriaDTO;
import com.epam.lab.dto.SortCriteriaDTO;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.TagRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceContextConfig.class, PersistenceContextConfig.class, TestContext.class})
@ActiveProfiles("dev")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class NewsServiceImplIntegrationTest {

    @Autowired
    private NewsService newsService;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private ModelMapper modelMapper;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void testSaveNews_ShouldRollBack() {
        Tag tag = new Tag();
        tag.setName("tag-test05");
        News news = new NewsBuilder()
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-28"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(new AuthorBuilder()
                        .withName("Name-test10")
                        .withSurname("Surname-test10")
                        .build())
                .withTag(tag)
                .build();
        int authorsCount = JdbcTestUtils.countRowsInTable(this.jdbcTemplate, "authors");
        int newsCount = JdbcTestUtils.countRowsInTable(this.jdbcTemplate, "news");

        try {
            newsService.save(modelMapper.map(news, NewsDTO.class));
        } catch (RuntimeException e) {
        }

        assertEquals(20, authorsCount);
        assertEquals(26, newsCount);
    }

    @Test
    public void testFindNews_ShouldPass() {
        List<SearchCriteriaDTO> searchCriteria = new ArrayList<>();
        searchCriteria.add(new SearchCriteriaDTO("author_name", ":", "Name-test01"));
        searchCriteria.add(new SearchCriteriaDTO("author_surname", ":", "Surname-test01"));
        List<SortCriteriaDTO> sortCriteria = new ArrayList<>();
        sortCriteria.add(new SortCriteriaDTO("creationDate", false));
        List<NewsDTO> news = newsService.findByCriteria(searchCriteria, sortCriteria);
        assertEquals(2, news.size());
        NewsDTO newsDTO1 = news.get(0);
        NewsDTO newsDTO2 = news.get(1);
        assertEquals(LocalDate.parse("2020-01-28"), newsDTO1.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-27"), newsDTO2.getCreationDate());
        assertEquals("Name-test01", newsDTO1.getAuthor().getName());
        assertEquals("Name-test01", newsDTO2.getAuthor().getName());
        assertEquals("Surname-test01", newsDTO1.getAuthor().getSurname());
        assertEquals("Surname-test01", newsDTO2.getAuthor().getSurname());
    }
}