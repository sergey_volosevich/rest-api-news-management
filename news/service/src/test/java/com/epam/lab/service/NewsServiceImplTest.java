package com.epam.lab.service;

import com.epam.lab.builder.AuthorBuilder;
import com.epam.lab.builder.NewsBuilder;
import com.epam.lab.builder.TagBuilder;
import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.Page;
import com.epam.lab.dto.Pagination;
import com.epam.lab.dto.SearchCriteriaDTO;
import com.epam.lab.dto.SortCriteriaDTO;
import com.epam.lab.dto.TagDTO;
import com.epam.lab.exception.ResourceNotFoundException;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.AuthorRepository;
import com.epam.lab.repository.NewsRepository;
import com.epam.lab.repository.SortSpecification;
import com.epam.lab.repository.Specification;
import com.epam.lab.repository.TagRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class NewsServiceImplTest {

    @InjectMocks
    private NewsServiceImpl newsService;

    @Mock
    private NewsRepository newsRepository;

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private TagRepository tagRepository;

    private NewsDTO newsDTO;

    @Before
    public void setUp() throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        newsService = new NewsServiceImpl(newsRepository, authorRepository, tagRepository, modelMapper);
        newsDTO = new NewsDTO();
        newsDTO.setTitle("Title-test01");
        newsDTO.setShortText("Short test test01");
        newsDTO.setFullText("Some text of news in test01");
        newsDTO.setCreationDate(LocalDate.of(2020, 1, 30));
        newsDTO.setModificationDate(LocalDate.of(2020, 1, 30));
        newsDTO.setAuthor(new AuthorDTO(1L, "Name-test01", "Surname-test01"));
        List<TagDTO> listTagDTO = new ArrayList<>();
        listTagDTO.add(new TagDTO(1L, "tag-test01"));
        newsDTO.setTags(new HashSet<>(listTagDTO));
    }

    @Test
    public void testSave_AuthorAndTagExist_ShouldPass() {
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        Tag tag = new TagBuilder()
                .withId(1L)
                .withName("tag-test01")
                .build();
        News news = new NewsBuilder()
                .withId(1L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        when(newsRepository.save(any(News.class))).thenReturn(news);
        NewsDTO savedNewsDTO = newsService.save(newsDTO);
        assertEquals(1L, savedNewsDTO.getId().longValue());
        assertEquals("Title", savedNewsDTO.getTitle());
        assertEquals("Short text", savedNewsDTO.getShortText());
        assertEquals("Some text of news", savedNewsDTO.getFullText());
        assertEquals(LocalDate.parse("2020-01-30"), savedNewsDTO.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-30"), savedNewsDTO.getModificationDate());
        verify(newsRepository, times(1)).save(any(News.class));
    }

    @Test
    public void testSave_AuthorAndTagNotExist_ShouldPass() {
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        Tag tag = new TagBuilder()
                .withId(1L)
                .withName("tag-test01")
                .build();
        News news = new NewsBuilder()
                .withId(1L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        newsDTO.getAuthor().setId(null);
        List<TagDTO> tags = new ArrayList<>(newsDTO.getTags());
        tags.get(0).setId(null);
        when(newsRepository.save(any(News.class))).thenReturn(news);
        when(tagRepository.save(any(Tag.class))).thenReturn(tag);
        when(authorRepository.save(any(Author.class))).thenReturn(author);
        NewsDTO savedNewsDTO = newsService.save(newsDTO);
        assertEquals(1L, savedNewsDTO.getId().longValue());
        assertEquals("Title", savedNewsDTO.getTitle());
        assertEquals("Short text", savedNewsDTO.getShortText());
        assertEquals("Some text of news", savedNewsDTO.getFullText());
        assertEquals(LocalDate.parse("2020-01-30"), savedNewsDTO.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-30"), savedNewsDTO.getModificationDate());
        assertEquals(1L, savedNewsDTO.getAuthor().getId().longValue());
        verify(newsRepository, times(1)).save(any(News.class));
        verify(authorRepository, times(1)).save(any(Author.class));
        verify(tagRepository, times(1)).save(any(Tag.class));
    }

    @Test
    public void testUpdate_AuthorAndTagExist_ShouldPass() {
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        Tag tag = new TagBuilder()
                .withId(1L)
                .withName("tag-test01")
                .build();
        News news = new NewsBuilder()
                .withId(1L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        when(newsRepository.update(any(News.class))).thenReturn(news);
        NewsDTO savedNewsDTO = newsService.update(newsDTO);
        assertEquals(1L, savedNewsDTO.getId().longValue());
        assertEquals("Title", savedNewsDTO.getTitle());
        assertEquals("Short text", savedNewsDTO.getShortText());
        assertEquals("Some text of news", savedNewsDTO.getFullText());
        assertEquals(LocalDate.parse("2020-01-30"), savedNewsDTO.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-30"), savedNewsDTO.getModificationDate());
        verify(newsRepository, times(1)).update(any(News.class));
    }

    @Test
    public void testUpdate_AuthorAndTagNotExist_ShouldPass() {
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        Tag tag = new TagBuilder()
                .withId(1L)
                .withName("tag-test01")
                .build();
        News news = new NewsBuilder()
                .withId(1L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        newsDTO.getAuthor().setId(null);
        List<TagDTO> tags = new ArrayList<>(newsDTO.getTags());
        tags.get(0).setId(null);
        when(newsRepository.update(any(News.class))).thenReturn(news);
        when(tagRepository.save(any(Tag.class))).thenReturn(tag);
        when(authorRepository.save(any(Author.class))).thenReturn(author);
        NewsDTO savedNewsDTO = newsService.update(newsDTO);
        assertEquals(1L, savedNewsDTO.getId().longValue());
        assertEquals("Title", savedNewsDTO.getTitle());
        assertEquals("Short text", savedNewsDTO.getShortText());
        assertEquals("Some text of news", savedNewsDTO.getFullText());
        assertEquals(LocalDate.parse("2020-01-30"), savedNewsDTO.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-30"), savedNewsDTO.getModificationDate());
        assertEquals(1L, savedNewsDTO.getAuthor().getId().longValue());
        verify(newsRepository, times(1)).update(any(News.class));
        verify(authorRepository, times(1)).save(any(Author.class));
        verify(tagRepository, times(1)).save(any(Tag.class));
    }

    @Test
    public void testDelete_ShouldPass() {
        doNothing().when(newsRepository).deleteById(1L);
        newsService.delete(1L);
        verify(newsRepository, times(1)).deleteById(1L);
        verifyNoMoreInteractions(newsRepository);
    }

    @Test
    public void testGetById_ShouldPass() {
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        Tag tag = new TagBuilder()
                .withId(1L)
                .withName("tag-test01")
                .build();
        News news = new NewsBuilder()
                .withId(1L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        when(newsRepository.findById(1L)).thenReturn(news);
        NewsDTO foundNews = newsService.getById(1L);
        assertEquals(1L, foundNews.getId().longValue());
        assertEquals("Title", foundNews.getTitle());
        assertEquals("Short text", foundNews.getShortText());
        assertEquals("Some text of news", foundNews.getFullText());
        assertEquals(LocalDate.parse("2020-01-30"), foundNews.getCreationDate());
        assertEquals(LocalDate.parse("2020-01-30"), foundNews.getModificationDate());
        assertEquals(1L, foundNews.getAuthor().getId().longValue());
        assertEquals(1, foundNews.getTags().size());
        verify(newsRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(newsRepository);

    }

    @Test
    public void testGetAll_ShouldPass() {
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        Tag tag = new TagBuilder()
                .withId(1L)
                .withName("tag-test01")
                .build();
        News news1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        News news2 = new NewsBuilder()
                .withId(2L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        List<News> news = new ArrayList<>();
        news.add(news1);
        news.add(news2);
        when(newsRepository.findAll()).thenReturn(news);
        List<NewsDTO> newsDTOS = newsService.getAll();
        assertEquals(2, newsDTOS.size());
    }

    @Test
    public void testFindByCriteria_ShouldPass() {
        List<SearchCriteriaDTO> searchCriteria = new ArrayList<>();
        searchCriteria.add(new SearchCriteriaDTO("author_name", ":", "Name-test01"));
        searchCriteria.add(new SearchCriteriaDTO("author_surname", ":", "Surname-test01"));
        List<SortCriteriaDTO> sortCriteria = new ArrayList<>();
        sortCriteria.add(new SortCriteriaDTO("creationDate", false));
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        Tag tag = new TagBuilder()
                .withId(1L)
                .withName("tag-test01")
                .build();
        News news1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        News news2 = new NewsBuilder()
                .withId(2L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        List<News> news = new ArrayList<>();
        news.add(news1);
        news.add(news2);
        when(newsRepository.findAll(any(Specification.class), any(SortSpecification.class)))
                .thenReturn(news);
        List<NewsDTO> foundNews = newsService.findByCriteria(searchCriteria, sortCriteria);
        assertEquals(2, foundNews.size());
    }

    @Test
    public void testGetNewsInParts_Valid_Pagination_ShouldPass() {
        Pagination pagination = new Pagination(5, 2);
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        Tag tag = new TagBuilder()
                .withId(1L)
                .withName("tag-test01")
                .build();
        News news1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        News news2 = new NewsBuilder()
                .withId(2L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        List<News> news = new ArrayList<>();
        news.add(news1);
        news.add(news2);
        when(newsRepository.findAll(anyInt(), anyInt())).thenReturn(news);
        when(newsRepository.countNews()).thenReturn(20L);
        Page<NewsDTO> page = newsService.getNewsInParts(pagination);
        assertEquals(20, page.getTotalCount());
        assertEquals(2, page.getEntities().size());
        assertEquals("limit:5;page:1;", page.getPreviousPage());
        assertEquals("limit:5;page:4;", page.getLastPage());
        assertEquals("limit:5;page:3;", page.getNextPage());
        verify(newsRepository, times(1)).findAll(anyInt(), anyInt());
        verify(newsRepository, times(1)).countNews();
        verifyNoMoreInteractions(newsRepository);
    }

    @Test
    public void testGetNewsInParts_NoResult_ShouldReturnEmptyPage() {
        Pagination pagination = new Pagination(5, 2);
        when(newsRepository.countNews()).thenReturn(0L);
        Page<NewsDTO> page = newsService.getNewsInParts(pagination);
        assertEquals(0, page.getTotalCount());
        assertEquals(0, page.getEntities().size());
        assertNull(page.getPreviousPage());
        assertNull(page.getLastPage());
        assertNull(page.getNextPage());
    }

    @Test
    public void testCountNews_ShouldReturnCount() {
        when(newsRepository.countNews()).thenReturn(10L);
        long count = newsService.countNews();
        assertEquals(10, count);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testGetNewsInParts_RequestedPageInvalid_ShouldThrowResourceNotFoundException() {
        Pagination pagination = new Pagination(6, 20);
        when(newsRepository.countNews()).thenReturn(10L);
        newsService.getNewsInParts(pagination);
    }

    @Test
    public void testFindByCriteria_WithPagination_ShouldReturnPartOfNews() {
        Pagination pagination = new Pagination(5, 2);
        List<SearchCriteriaDTO> searchCriteria = new ArrayList<>();
        searchCriteria.add(new SearchCriteriaDTO("author_name", ":", "Name-test01"));
        searchCriteria.add(new SearchCriteriaDTO("author_surname", ":", "Surname-test01"));
        List<SortCriteriaDTO> sortCriteria = new ArrayList<>();
        sortCriteria.add(new SortCriteriaDTO("creationDate", false));
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        Tag tag = new TagBuilder()
                .withId(1L)
                .withName("tag-test01")
                .build();
        News news1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        News news2 = new NewsBuilder()
                .withId(2L)
                .withTitle("Title")
                .withShortText("Short text")
                .withFullText("Some text of news")
                .withCreationDate(LocalDate.parse("2020-01-30"))
                .withModificationDate(LocalDate.parse("2020-01-30"))
                .withAuthor(author)
                .withTag(tag)
                .build();
        List<News> news = new ArrayList<>();
        news.add(news1);
        news.add(news2);
        when(newsRepository.findAll(any(Specification.class), any(SortSpecification.class), anyInt(),
                anyInt())).thenReturn(news);
        when(newsRepository.countFoundEntities(any(Specification.class))).thenReturn(20L);
        Page<NewsDTO> page = newsService.findByCriteria(searchCriteria, sortCriteria, pagination);
        assertEquals(20, page.getTotalCount());
        assertEquals(2, page.getEntities().size());
        assertEquals("limit:5;page:1;", page.getPreviousPage());
        assertEquals("limit:5;page:4;", page.getLastPage());
        assertEquals("limit:5;page:3;", page.getNextPage());
        verify(newsRepository, times(1)).countFoundEntities(any(Specification.class));
        verify(newsRepository, times(1)).findAll(any(Specification.class),
                any(SortSpecification.class), anyInt(), anyInt());
        verifyNoMoreInteractions(newsRepository);
    }

    @Test
    public void testFindByCriteria_NoResult_ShouldReturnEmptyPage() {
        Pagination pagination = new Pagination(5, 2);
        List<SearchCriteriaDTO> searchCriteria = new ArrayList<>();
        searchCriteria.add(new SearchCriteriaDTO("author_name", ":", "Name-test01"));
        searchCriteria.add(new SearchCriteriaDTO("author_surname", ":", "Surname-test01"));
        List<SortCriteriaDTO> sortCriteria = new ArrayList<>();
        sortCriteria.add(new SortCriteriaDTO("creationDate", false));
        when(newsRepository.countFoundEntities(any(Specification.class))).thenReturn(0L);
        Page<NewsDTO> page = newsService.findByCriteria(searchCriteria, sortCriteria, pagination);
        assertEquals(0, page.getTotalCount());
        assertEquals(0, page.getEntities().size());
        assertNull(page.getPreviousPage());
        assertNull(page.getLastPage());
        assertNull(page.getNextPage());
    }
}