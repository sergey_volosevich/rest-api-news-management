package com.epam.lab.service;

import com.epam.lab.builder.AuthorBuilder;
import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.model.Author;
import com.epam.lab.repository.AuthorRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Mock
    private AuthorRepository authorRepository;

    private AuthorDTO authorDTO;

    @Before
    public void setUp() throws Exception {
        authorService = new AuthorServiceImpl(authorRepository, new ModelMapper());
        authorDTO = new AuthorDTO();
        authorDTO.setName("Name-Test01");
        authorDTO.setSurname("Surname-test01");
    }

    @Test
    public void testSave_ShouldPass() {
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-Test01")
                .withSurname("Surname-test01")
                .build();
        when(authorRepository.save(any(Author.class))).thenReturn(author);
        AuthorDTO savedAuthor = authorService.save(authorDTO);
        verify(authorRepository, times(1)).save(any(Author.class));
        assertEquals(1L, savedAuthor.getId().longValue());
        assertEquals("Name-Test01", savedAuthor.getName());
        assertEquals("Surname-test01", savedAuthor.getSurname());
    }

    @Test
    public void testUpdate_ShouldPass() {
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Sergei")
                .withSurname("Surname-test01")
                .build();
        when(authorRepository.update(any(Author.class))).thenReturn(author);
        AuthorDTO updateAuthor = authorService.update(authorDTO);
        verify(authorRepository, times(1)).update(any(Author.class));
        assertEquals(1L, updateAuthor.getId().longValue());
        assertEquals("Sergei", updateAuthor.getName());
        assertEquals("Surname-test01", updateAuthor.getSurname());
    }

    @Test
    public void testDelete_ByAuthorExist_ShouldPass() {
        doNothing().when(authorRepository).deleteById(1L);
        authorService.delete(1L);
        verify(authorRepository, times(1)).deleteById(1L);
    }

    @Test
    public void testGetById_ValidId() {
        Author author = new AuthorBuilder()
                .withId(1L)
                .withName("Name-Test01")
                .withSurname("Surname-test01")
                .build();
        when(authorRepository.findById(1L)).thenReturn(author);
        AuthorDTO foundAuthor = authorService.getById(1);
        verify(authorRepository, times(1)).findById(1L);
        assertEquals(1L, author.getId().longValue());
        assertEquals("Name-Test01", author.getName());
        assertEquals("Surname-test01", author.getSurname());
    }

    @Test
    public void testGetAll_ShouldPass() {
        Author author1 = new AuthorBuilder()
                .withId(1L)
                .withName("Name-Test01")
                .withSurname("Surname-test01")
                .build();
        Author author2 = new AuthorBuilder()
                .withId(2L)
                .withName("Name-Test02")
                .withSurname("Surname-test02")
                .build();
        List<Author> authors = new ArrayList<>();
        authors.add(author1);
        authors.add(author2);
        when(authorRepository.findAll()).thenReturn(authors);
        List<AuthorDTO> authorDTOS = authorService.getAll();
        assertEquals(2, authorDTOS.size());
        AuthorDTO authorDTO1 = authorDTOS.get(0);
        AuthorDTO authorDTO2 = authorDTOS.get(1);
        assertEquals(1L, authorDTO1.getId().longValue());
        assertEquals("Name-Test01", authorDTO1.getName());
        assertEquals("Surname-test01", authorDTO1.getSurname());
        assertEquals(2L, authorDTO2.getId().longValue());
        assertEquals("Name-Test02", authorDTO2.getName());
        assertEquals("Surname-test02", authorDTO2.getSurname());
    }
}