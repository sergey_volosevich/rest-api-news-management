package com.epam.lab.service;

import com.epam.lab.builder.TagBuilder;
import com.epam.lab.dto.TagDTO;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.TagRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

    @InjectMocks
    private TagServiceImpl tagService;

    @Mock
    private TagRepository tagRepository;

    private TagDTO tagDTO;

    @Before
    public void setUp() throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        tagService = new TagServiceImpl(tagRepository, modelMapper);
        tagDTO = new TagDTO();
        tagDTO.setName("Tag-test01");
    }

    @Test
    public void testSave_ShouldPass() {
        Tag tag = new Tag(1L, "Tag-test01");
        when(tagRepository.save(any(Tag.class))).thenReturn(tag);
        TagDTO savedTag = tagService.save(tagDTO);
        verify(tagRepository, times(1)).save(any(Tag.class));
        assertEquals(1L, savedTag.getId().longValue());
        assertEquals("Tag-test01", savedTag.getName());
    }

    @Test
    public void testUpdate_ShouldPass() {
        Tag tag = new Tag(1L, "Tag-test01");
        when(tagRepository.update(any(Tag.class))).thenReturn(tag);
        TagDTO updatedTag = tagService.update(tagDTO);
        verify(tagRepository, times(1)).update(any(Tag.class));
        assertEquals(1L, updatedTag.getId().longValue());
        assertEquals("Tag-test01", updatedTag.getName());
    }

    @Test
    public void testDelete_ShouldPass() {
        doNothing().when(tagRepository).deleteById(1L);
        tagService.delete(1L);
        verify(tagRepository, times(1)).deleteById(1L);
    }

    @Test
    public void tetGetById_ShouldPass() {
        Tag tag = new Tag(1L, "Tag-test01");
        when(tagRepository.findById(1L)).thenReturn(tag);
        TagDTO foundTag = tagService.getById(1);
        verify(tagRepository, times(1)).findById(1L);
        assertEquals(1L, foundTag.getId().longValue());
        assertEquals("Tag-test01", foundTag.getName());
    }

    @Test
    public void testGetAll_ShouldPass() {
        Tag tag1 = new TagBuilder()
                .withId(1L)
                .withName("tag-test01")
                .build();
        Tag tag2 = new TagBuilder()
                .withId(2L)
                .withName("tag-test02")
                .build();
        List<Tag> tags = new ArrayList<>();
        tags.add(tag1);
        tags.add(tag2);
        when(tagRepository.findAll()).thenReturn(tags);
        List<TagDTO> foundTgs = tagService.getAll();
        assertEquals(2, foundTgs.size());
    }
}