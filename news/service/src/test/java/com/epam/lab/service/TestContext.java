package com.epam.lab.service;

import com.epam.lab.repository.TagRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Proxy;

@Configuration
public class TestContext {

    @Bean
    public TagRepository tagRepository(TagRepository tagRepository) {
        return (TagRepository) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(),
                tagRepository.getClass().getInterfaces(),
                (proxy, method, args) -> {
                    if ("save".equals(method.getName())) {
                        throw new RuntimeException();
                    }
                    return method.invoke(tagRepository, args);
                });
    }
}
