package com.epam.lab.builder;

import com.epam.lab.model.Tag;

public class TagBuilder {
    private Long id;
    private String name;

    public TagBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public TagBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public Tag build() {
        return new Tag(id, name);
    }
}
