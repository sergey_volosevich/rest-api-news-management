package com.epam.lab.builder;

import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class NewsBuilder {

    private Long id;
    private String title;
    private String shortText;
    private String fullText;
    private LocalDate creationDate;
    private LocalDate modificationDate;
    private Author author;
    private Set<Tag> tags;

    public NewsBuilder() {
        tags = new HashSet<>();
    }

    public NewsBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public NewsBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public NewsBuilder withShortText(String shortText) {
        this.shortText = shortText;
        return this;
    }

    public NewsBuilder withFullText(String fullText) {
        this.fullText = fullText;
        return this;
    }

    public NewsBuilder withCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public NewsBuilder withModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
        return this;
    }

    public NewsBuilder withAuthor(Author author) {
        this.author = author;
        return this;
    }

    public NewsBuilder withTag(Tag tag) {
        this.tags.add(tag);
        return this;
    }

    public News build() {
        return new News(id, title, shortText, fullText, creationDate, modificationDate, author, tags);
    }

}
