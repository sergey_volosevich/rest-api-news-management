INSERT INTO authors (name, surname)
VALUES ('Name-one', 'Surname-one'),
       ('Name-two', 'Surname-two'),
       ('Name-three', 'Surname-three'),
       ('Name-four', 'Surname-four'),
       ('Name-five', 'Surname-five'),
       ('Name-six', 'Surname-six'),
       ('Name-seven', 'Surname-seven'),
       ('Name-eight', 'Surname-eight'),
       ('Name-nine', 'Surname-nine'),
       ('Name-ten', 'Surname-ten'),
       ('Name-eleven', 'Surname-eleven'),
       ('Name-twelve', 'Surname-twelve'),
       ('Name-thirteen', 'Surname-thirteen'),
       ('Name-fourteen', 'Surname-fourteen'),
       ('Name-fifteen', 'Surname-fifteen'),
       ('Name-sixteen', 'Surname-sixteen'),
       ('Name-seventeen', 'Surname-seventeen'),
       ('Name-eighteen', 'Surname-eighteen'),
       ('Name-nineteen', 'Surname-nineteen'),
       ('Name-twenty', 'Surname-twenty');


INSERT INTO news (title, short_text, full_text, creation_date,  modification_date)
VALUES ('Title-test01', 'Short text in test01',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis.' ||
'Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
              '2020-01-28', '2020-01-28'),
       ('Title-test02', 'Short text in test02',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
       '2020-01-27', '2020-01-29'),
       ('Title-test03', 'Short text in test03',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-29', '2020-01-30'),
       ('Title-test04', 'Short text in test04',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-27', '2020-01-31'),
       ('Title-test05', 'Short text in test05',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-26', '2020-01-29'),
       ('Title-test06', 'Short text in test06',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-28', '2020-01-30'),
       ('Title-test07', 'Short text in test07',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-28', '2020-01-29'),
       ('Title-test08', 'Short text in test08',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-28', '2020-01-28'),
       ('Title-test09', 'Short text in test09',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-28', '2020-01-28'),
       ('Title-test10', 'Short text in test10',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-23', '2020-01-28'),
       ('Title-test11', 'Short text in test11',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-24', '2020-01-28'),
       ('Title-test12', 'Short text in test12',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-25', '2020-01-28'),
       ('Title-test13', 'Short text in test13',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
        '2020-01-27', '2020-01-28'),
       ('Title-test14', 'Short text in test14',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-28', '2020-01-30'),
       ('Title-test15', 'Short text in test15',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-28', '2020-01-30'),
       ('Title-test16', 'Short text in test16',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-25', '2020-01-31'),
       ('Title-test17', 'Short text in test17',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-21', '2020-01-28'),
       ('Title-test18', 'Short text in test18',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-28', '2020-01-28'),
       ('Title-test19', 'Short text in test19',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-23', '2020-01-28'),
       ('Title-test20', 'Short text in test20',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
        '2020-01-25', '2020-01-29'),
       ('Title-test21', 'Short text in test21',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-28', '2020-01-28'),
       ('Title-test22', 'Short text in test22',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-23', '2020-01-27'),
       ('Title-test23', 'Short text in test23',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-28', '2020-01-28'),
       ('Title-test24', 'Short text in test24',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-27', '2020-01-28'),
       ('Title-test25', 'Short text in test25',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-28', '2020-01-28'),
       ('Title-test26', 'Short text in test26',
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium mattis enim id auctor. Proin porttitor magna' ||
' quis leo consequat, vel dapibus sapien porttitor. Cras et porta felis. In quis erat tempor, placerat purus ut, ' ||
'lacinia nisi. Aenean laoreet sodales metus lobortis suscipit. Duis commodo ligula et convallis tincidunt. ' ||
'Suspendisse sollicitudin tellus est, et feugiat mauris tincidunt in. Phasellus cursus mattis velit, ac commodo ' ||
'purus tempus nec. In sagittis maximus dictum. Quisque porttitor elementum purus. Integer maximus ante non' ||
' consequat eleifend. Fusce facilisis dui vel dui ultrices, ut feugiat nibh malesuada. Phasellus dignissim arcu' ||
' nec eros aliquet facilisis. Aliquam rutrum, leo non luctus luctus, nisl mi fringilla velit, eget placerat ' ||
'justo nulla et lectus. Integer elit dolor, accumsan non blandit sed, vehicula in est. Suspendisse ornare leo ' ||
'tempor urna ornare vestibulum. Mauris dignissim aliquet enim, quis fringilla sapien. Phasellus tempor, ipsum ' ||
'vitae posuere finibus, sem neque tincidunt dui, eget rutrum elit ipsum a dui. Nunc vitae efficitur odio, ' ||
'ut lobortis purus. Donec vitae viverra enim. Nullam et suscipit augue. Maecenas quis congue elit. Praesent' ||
' scelerisque varius nisi ut sagittis. Duis a scelerisque ex, vitae tempor risus. Integer imperdiet ac enim' ||
' id luctus. Curabitur et dolor ut lorem ullamcorper molestie nec et augue. Lorem ipsum dolor sit amet,' ||
' consectetur adipiscing elit.',
         '2020-01-30', '2020-01-30');

INSERT INTO tags (name)
VALUES ('tag_test01'),
       ('tag_test02'),
       ('tag_test03'),
       ('tag_test04'),
       ('tag_test05'),
       ('tag_test06'),
       ('tag_test07'),
       ('tag_test08'),
       ('tag_test09'),
       ('tag_test10'),
       ('tag_test11'),
       ('tag_test12'),
       ('tag_test13'),
       ('tag_test14'),
       ('tag_test15'),
       ('tag_test16'),
       ('tag_test17'),
       ('tag_test18'),
       ('tag_test19'),
       ('tag_test20');

INSERT INTO news_author
VALUES (1, 1),
       (2, 1),
       (3, 2),
       (4, 2),
       (5, 4),
       (6, 5),
       (7, 6),
       (8, 7),
       (9, 8),
       (10, 9),
       (11, 10),
       (12, 11),
       (13, 12),
       (14, 13),
       (15, 14),
       (16, 15),
       (17, 16),
       (18, 17),
       (19, 18),
       (20, 19),
       (21, 20),
       (22, 20),
       (23, 17),
       (24, 19),
       (25, 5),
       (26, 6);

INSERT INTO news_tag
VALUES (1, 1),
       (2, 1),
       (3, 2),
       (4, 2),
       (5, 3),
       (6, 3),
       (7, 4),
       (8, 5),
       (9, 6),
       (10, 7),
       (11, 8),
       (12, 9),
       (13, 10),
       (14, 11),
       (15, 12),
       (16, 13),
       (17, 14),
       (18, 15),
       (19, 16),
       (20, 17),
       (21, 18),
       (22, 19),
       (23, 20),
       (24, 5),
       (25, 5),
       (26, 7),
       (3, 1),
       (1, 2);
INSERT INTO users (name, surname, login, password)
VALUES ('Name-one', 'Surname-one','admin','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-two', 'Surname-two','test02','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-three', 'Surname-three','test03','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-four', 'Surname-four','test04','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-five', 'Surname-five','test05','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-six', 'Surname-six','test06','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-seven', 'Surname-seven','test07','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-eight', 'Surname-eight','test08','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-nine', 'Surname-nine','test09','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-ten', 'Surname-ten','test10','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-eleven', 'Surname-eleven','test11','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-twelve', 'Surname-twelve','test12','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-thirteen', 'Surname-thirteen','test13','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-fourteen', 'Surname-fourteen','test14','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-fifteen', 'Surname-fifteen','test15','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-sixteen', 'Surname-sixteen','test16','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-seventeen', 'Surname-seventeen','test17','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-eighteen', 'Surname-eighteen','test18','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-nineteen', 'Surname-nineteen','test19','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76'),
       ('Name-twenty', 'Surname-twenty','test20','$2a$16$G0AwZY/Q1OevDItivsTvPOHLXHYZwbQhsf30qvR16Amhkkqd91Y76');

INSERT INTO roles (role_name)
VALUES ('ADMIN'),
       ('USER');

INSERT INTO user_roles
VALUES (1,1),
       (2,2),
       (3,2),
       (4,2),
       (5,2),
       (6,2),
       (7,2),
       (8,2),
       (9,2),
       (10,2),
       (11,2),
       (12,2),
       (13,2),
       (14,2),
       (15,2),
       (16,2),
       (17,2),
       (18,2),
       (19,2),
       (20,2);