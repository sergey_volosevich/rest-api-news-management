package com.epam.lab.controller;

import com.epam.lab.exception.CriteriaNotFoundException;
import com.epam.lab.exception.RepositoryException;
import com.epam.lab.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = LogManager.getLogger(ControllerExceptionHandler.class);
    private static final HttpStatus BAD_REQUEST_STATUS = HttpStatus.BAD_REQUEST;
    private static final String FAILED_TO_EXECUTE_METHOD_CAUSE = "Failed to execute method. Cause: ";
    private static final String METHOD_NOT_SUPPORTED_MSG = "Method {0} not supported. Supported methods: {1}";
    private static final String NO_HANDLER_FOUND_FOR = "No handler found for {0} {1}";
    private static final String MALFORMED_JSON_REQUEST = "Malformed JSON request";
    private static final String ERROR_OCCURRED = "Ops! Something go wrong. Try again later.";

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        LOG.error(FAILED_TO_EXECUTE_METHOD_CAUSE, ex);
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        ApiErrorResponse response = buildResponseMessage(status, errors);
        return new ResponseEntity<>(response.getResponseBody(), headers, status);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex) {
        LOG.error(FAILED_TO_EXECUTE_METHOD_CAUSE, ex);
        List<String> errors = ex.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
        ApiErrorResponse apiErrorResponse = buildResponseMessage(BAD_REQUEST_STATUS, errors);
        return new ResponseEntity<>(apiErrorResponse.getResponseBody(), new HttpHeaders(), BAD_REQUEST_STATUS);
    }

    @ExceptionHandler(CriteriaNotFoundException.class)
    public ResponseEntity<Object> handleCriteriaNotFound(CriteriaNotFoundException ex) {
        LOG.error(FAILED_TO_EXECUTE_METHOD_CAUSE, ex);
        ApiErrorResponse apiErrorResponse = buildResponseMessage(BAD_REQUEST_STATUS, ex.getMessage());
        return new ResponseEntity<>(apiErrorResponse.getResponseBody(), new HttpHeaders(), BAD_REQUEST_STATUS);
    }

    @ExceptionHandler(RepositoryException.class)
    public ResponseEntity<Object> handleSearchParameterNotFoundException(RepositoryException ex) {
        LOG.error(FAILED_TO_EXECUTE_METHOD_CAUSE, ex);
        ApiErrorResponse apiErrorResponse = buildResponseMessage(BAD_REQUEST_STATUS, ex.getMessage());
        return new ResponseEntity<>(apiErrorResponse.getResponseBody(), new HttpHeaders(), BAD_REQUEST_STATUS);
    }

    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<Object> handleResourceNotFoundException(ServiceException ex) {
        LOG.error(FAILED_TO_EXECUTE_METHOD_CAUSE, ex);
        ApiErrorResponse apiErrorResponse = buildResponseMessage(HttpStatus.NOT_FOUND, ex.getMessage());
        return new ResponseEntity<>(apiErrorResponse.getResponseBody(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<Object> handleResponseStatusException(ResponseStatusException ex) {
        LOG.error(FAILED_TO_EXECUTE_METHOD_CAUSE, ex);
        HttpStatus status = ex.getStatus();
        ApiErrorResponse apiErrorResponse = buildResponseMessage(status, ex.getReason());
        return new ResponseEntity<>(apiErrorResponse.getResponseBody(), new HttpHeaders(), status);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
                                                                   HttpStatus status, WebRequest request) {
        LOG.error(FAILED_TO_EXECUTE_METHOD_CAUSE, ex);
        String errorMessage = MessageFormat.format(NO_HANDLER_FOUND_FOR, ex.getHttpMethod(), ex.getRequestURL());
        ApiErrorResponse apiErrorResponse = buildResponseMessage(HttpStatus.NOT_FOUND, errorMessage);
        return new ResponseEntity<>(apiErrorResponse.getResponseBody(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        LOG.error(FAILED_TO_EXECUTE_METHOD_CAUSE, ex);
        HttpStatus internalServerErrorStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        ApiErrorResponse apiErrorResponse = buildResponseMessage(internalServerErrorStatus, ERROR_OCCURRED);
        return new ResponseEntity<>(apiErrorResponse.getResponseBody(), new HttpHeaders(), internalServerErrorStatus);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        LOG.error(FAILED_TO_EXECUTE_METHOD_CAUSE, ex);
        ApiErrorResponse apiErrorResponse = buildResponseMessage(status, MALFORMED_JSON_REQUEST);
        return new ResponseEntity<>(apiErrorResponse.getResponseBody(), headers, status);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
                                                                         HttpHeaders headers, HttpStatus status,
                                                                         WebRequest request) {
        LOG.error(FAILED_TO_EXECUTE_METHOD_CAUSE, ex);
        String unsupportedMethodName = ex.getMethod();
        StringJoiner joiner = new StringJoiner(", ");
        Objects.requireNonNull(ex.getSupportedHttpMethods()).forEach(method -> joiner.add(method.name()));
        String msg = MessageFormat.format(METHOD_NOT_SUPPORTED_MSG, unsupportedMethodName, joiner);
        ApiErrorResponse response = buildResponseMessage(status, msg);
        return new ResponseEntity<>(response.getResponseBody(), headers, status);
    }

    private ApiErrorResponse buildResponseMessage(HttpStatus status, List<String> errors) {
        return new ApiErrorResponse
                .ResponseBodyBuilder()
                .atTime(LocalDate.now().toString())
                .withStatus(status)
                .withErrorCode(status.value())
                .withMessage(errors)
                .build();
    }

    private ApiErrorResponse buildResponseMessage(HttpStatus status, String error) {
        return new ApiErrorResponse
                .ResponseBodyBuilder()
                .atTime(LocalDate.now().toString())
                .withStatus(status)
                .withErrorCode(status.value())
                .withMessage(error)
                .build();
    }
}
