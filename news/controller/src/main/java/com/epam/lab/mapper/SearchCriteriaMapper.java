package com.epam.lab.mapper;

import com.epam.lab.dto.SearchCriteriaDTO;

import java.util.List;

public interface SearchCriteriaMapper {
    List<SearchCriteriaDTO> mapToSearchCriteria(String query);

}
