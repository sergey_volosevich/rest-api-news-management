package com.epam.lab.mapper;

import com.epam.lab.dto.SortCriteriaDTO;
import com.epam.lab.exception.CriteriaNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class NewsSortCriteriaMapper implements SortCriteriaMapper {

    private static final Logger LOGGER = LogManager.getLogger(NewsSortCriteriaMapper.class);

    private static final String FAILED_TO_PARSE_QUERY = "Failed to parse query";
    private static final String PROVIDE_VALID_PARAMETER_VALUE_FOR_SORT = "Provide valid parameters for sort. Invalid "
            + "value for parameter {0}";
    private static final String PROVIDE_VALID_PARAMETERS_FOR_SORT = "Provide valid parameters for sort. Can not find "
            + "sort parameter with name {0}";
    private static final String ASCENDING_ORDER = "asc";
    private static final String DESCENDING_ORDER = "desc";
    private static final String INVALID_VALUE_FOR_SORT_CRITERIA = "Invalid value for sort criteria.";
    private static final String SORT_PARAMETER_PATTERN = "(\\w+?)(:)(\\w+[-]*\\w+);";
    private static final String INVALID_REQUEST_PARAMETERS = "Provide valid parameters for search. " +
            "Invalid parameters syntax.";

    @Override
    public List<SortCriteriaDTO> mapToSortCriteria(String query) {
        List<SortCriteriaDTO> criteriaList = new ArrayList<>();
        Matcher matcher = getMatcher(query);
        while (matcher.find()) {
            String columnName = matcher.group(1);
            String filedValue = matcher.group(3);
            NewsFieldName newsFieldName = getNewsFieldName(columnName);
            if (filedValue.equalsIgnoreCase(ASCENDING_ORDER)) {
                criteriaList.add(new SortCriteriaDTO(newsFieldName.getNameOfClassField(), true));
            } else if (filedValue.equalsIgnoreCase(DESCENDING_ORDER)) {
                criteriaList.add(new SortCriteriaDTO(newsFieldName.getNameOfClassField(), false));
            } else {
                LOGGER.error(INVALID_VALUE_FOR_SORT_CRITERIA);
                throw new CriteriaNotFoundException(MessageFormat.format(PROVIDE_VALID_PARAMETER_VALUE_FOR_SORT,
                        columnName));
            }
        }
        if (criteriaList.isEmpty()) {
            throw new CriteriaNotFoundException(INVALID_REQUEST_PARAMETERS);
        }
        return criteriaList;
    }

    private NewsFieldName getNewsFieldName(String columnName) {
        Optional<NewsFieldName> columnNameOptional = NewsFieldName.getColumnNameFromString(columnName);
        if (!columnNameOptional.isPresent()) {
            LOGGER.error(FAILED_TO_PARSE_QUERY);
            throw new CriteriaNotFoundException(MessageFormat.format(PROVIDE_VALID_PARAMETERS_FOR_SORT, columnName));
        }
        return columnNameOptional.get();
    }

    private static Matcher getMatcher(String queryString) {
        Pattern pattern = Pattern.compile(NewsSortCriteriaMapper.SORT_PARAMETER_PATTERN);
        return pattern.matcher(queryString);
    }
}
