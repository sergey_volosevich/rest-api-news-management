package com.epam.lab.mapper;

import java.util.Optional;
import java.util.stream.Stream;

public enum NewsFieldName {
    TITLE("title", "title"), SHORT_TEXT("shortText",
            "shortText"), CREATION_DATE("creationDate", "creationDate"),
    MODIFICATION_DATE("modificationDate", "modificationDate"),
    AUTHOR_NAME("authorName", "author_name"),
    AUTHOR_SURNAME("authorSurname", "author_surname"),
    TAG_NAME("tag", "tags_name");

    private String columnName;
    private String nameOfClassField;

    NewsFieldName(String columnName, String nameOfClassField) {
        this.columnName = columnName;
        this.nameOfClassField = nameOfClassField;
    }

    public static Optional<NewsFieldName> getColumnNameFromString(String param) {
        return Stream.of(NewsFieldName.values()).filter(name -> name.columnName.equalsIgnoreCase(param)).findFirst();
    }

    public String getNameOfClassField() {
        return nameOfClassField;
    }
}
