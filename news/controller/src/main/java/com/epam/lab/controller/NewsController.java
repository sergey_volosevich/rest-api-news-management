package com.epam.lab.controller;

import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.Page;
import com.epam.lab.dto.Pagination;
import com.epam.lab.dto.SearchCriteriaDTO;
import com.epam.lab.dto.SortCriteriaDTO;
import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.mapper.SearchCriteriaMapper;
import com.epam.lab.mapper.SortCriteriaMapper;
import com.epam.lab.service.NewsService;
import com.epam.lab.transfer.NewsTransfer;
import com.epam.lab.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.Pattern;
import java.net.URI;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * This class is used for working with {@link NewsDTO}. Create, read, update and delete, search method are represented
 * in this class.
 */


@RestController
@RequestMapping(value = "/news")
@Validated
public class NewsController {

    private static final String NUMBER_PATTERN = "^[1-9]\\d*$";
    private static final String NEXT_LINK = "next";
    private static final String LATS_LINK = "last";
    private static final String PREVIOUS_LINK = "previous";
    private static final String NEWS_NOT_FOUND = "News not found";
    private static final String NEWS_SORT_CRITERIA_MAPPER = "newsSortCriteriaMapper";
    private static final String NEWS_SEARCH_CRITERIA_MAPPER = "newsSearchCriteriaMapper";
    private static final String SORT_QUERY_PATTERN = "^((\\w+?)(:)(\\w+[-]*\\w.+);)*$";
    private static final String SEARCH_QUERY_PATTERN = "^((\\w+?)([:><])(\\w+[-]*\\w.+);)*$";
    private static final String X_TOTAL_COUNT_HEADER = "X-Total-Count";
    private static final String VALIDATION_MSG_FOR_ID = "Invalid request";
    private static final String INCORRECT_QUERY_VALUE = "Incorrect query parameter values";
    private static final String FAILED_TO_DELETE_NEWS = "Failed to delete news. Caused: can not find news with"
            + " id = {0}";
    private static final String PAGINATION_QUERY_PATTERN = "^((limit|page)([:])([1-9]\\d*);)+$";

    private final NewsService newsService;

    private final Map<String, SearchCriteriaMapper> searchMappers;

    private final Map<String, SortCriteriaMapper> sortMappers;

    @Autowired
    public NewsController(NewsService newsService, Map<String, SearchCriteriaMapper> searchMappers,
                          Map<String, SortCriteriaMapper> sortMappers) {
        this.newsService = newsService;
        this.searchMappers = searchMappers;
        this.sortMappers = sortMappers;
    }

    /**
     * This method gets and displayed {@link NewsDTO}
     *
     * @param id unique identifier of {@link NewsDTO}
     * @return {@link NewsDTO} representation or throw {@link com.epam.lab.exception.EntityNotFoundException} if
     * does not exist.
     */

    @GetMapping(value = "/{id}")
    public NewsDTO getNews(@PathVariable("id") @Pattern(regexp = NUMBER_PATTERN,
            message = VALIDATION_MSG_FOR_ID, flags = Pattern.Flag.CASE_INSENSITIVE) String id) {
        try {
            return newsService.getById(Long.parseLong(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, NEWS_NOT_FOUND, e);
        }
    }

    /**
     * This method is used to create {@link NewsDTO}
     *
     * @param newsDTO contains all information to save
     * @param ucb     {@link UriComponentsBuilder} need to build URI for Header Location.
     * @return {@link NewsDTO} representation
     */

    @PostMapping()
    public ResponseEntity<NewsDTO> saveNews(@Validated(NewsTransfer.New.class) @RequestBody NewsDTO newsDTO,
                                            UriComponentsBuilder ucb) {
        NewsDTO savedNews = newsService.save(newsDTO);
        long id = savedNews.getId();
        HttpHeaders headers = new HttpHeaders();
        URI locationUrl = RequestUtil.buildLocationURL(ucb, "/news/", String.valueOf(id));
        RequestUtil.addLocationUrlToHeader(headers, locationUrl);
        return new ResponseEntity<>(savedNews, headers, HttpStatus.CREATED);
    }

    /**
     * This method is used to update concrete {@link NewsDTO}.
     *
     * @param id        unique identifier of {@link NewsDTO}
     * @param newsDTO   contains all information to update
     * @param ucb{@link UriComponentsBuilder} need to build URI for Header Location.
     * @return {@link NewsDTO} representation or throw {@link com.epam.lab.exception.EntityNotFoundException} if
     * does not exist.
     */

    @PutMapping(value = "/{id}")
    public ResponseEntity<NewsDTO> updateNews(@PathVariable("id")
                                              @Pattern(regexp = NUMBER_PATTERN, message = VALIDATION_MSG_FOR_ID,
                                                      flags = Pattern.Flag.CASE_INSENSITIVE) String id,
                                              @Validated(NewsTransfer.UpdateNews.class)
                                              @RequestBody NewsDTO newsDTO, UriComponentsBuilder ucb) {
        try {
            NewsDTO updatedNews = newsService.update(newsDTO);
            return new ResponseEntity<>(updatedNews, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

    }

    /**
     * This method is used to delete concrete {@link NewsDTO}
     *
     * @param id unique identifier of {@link NewsDTO}
     */

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeNews(@PathVariable("id") @Pattern(regexp = NUMBER_PATTERN,
            message = VALIDATION_MSG_FOR_ID, flags = Pattern.Flag.CASE_INSENSITIVE) String id) {
        try {
            newsService.delete(Long.parseLong(id));
        } catch (EntityNotFoundException e) {
            String msg = MessageFormat.format(FAILED_TO_DELETE_NEWS, id);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg, e);
        }
    }

    /**
     * This method is used to get all news from database
     *
     * @return java.util.List of {@link NewsDTO}
     */

    @GetMapping
    public ResponseEntity<List<NewsDTO>> getAllNews() {
        HttpHeaders httpHeaders = new HttpHeaders();
        List<NewsDTO> news = newsService.getAll();
        httpHeaders.add(X_TOTAL_COUNT_HEADER, String.valueOf(news.size()));
        return new ResponseEntity<>(news, httpHeaders, HttpStatus.OK);
    }

    /**
     * This method is used to get all {@link NewsDTO} in several parts
     *
     * @param pag pagination parameter, includes two sub parameters: limit - how many {@link NewsDTO} must be in
     *            the response, page - what part of java.util.List {@link NewsDTO} must be returned.
     *            Example: ?pag=limit:5;page:1;
     * @return java.util.List of {@link NewsDTO}
     */

    @GetMapping(params = {"pag"})
    public ResponseEntity<List<NewsDTO>> getAllNews(@Pattern(regexp = PAGINATION_QUERY_PATTERN,
            message = INCORRECT_QUERY_VALUE, flags = Pattern.Flag.CASE_INSENSITIVE)
                                                    @RequestParam(value = "pag") String pag) {
        Pagination pagination = RequestUtil.mapPaginationFromRequest(pag);
        Page<NewsDTO> page = newsService.getNewsInParts(pagination);
        HttpHeaders httpHeaders = new HttpHeaders();
        List<String> headerValues = new ArrayList<>();
        if (Objects.nonNull(page.getNextPage())) {
            Link next = linkTo(methodOn(NewsController.class).getAllNews(page.getNextPage())).withRel(NEXT_LINK);
            headerValues.add(next.toString());
        }
        if (Objects.nonNull(page.getLastPage())) {
            Link last = linkTo(methodOn(NewsController.class).getAllNews(page.getLastPage())).withRel(LATS_LINK);
            headerValues.add(last.toString());
        }
        if (Objects.nonNull(page.getPreviousPage())) {
            Link previous = linkTo(methodOn(NewsController.class).getAllNews(page.getPreviousPage()))
                    .withRel(PREVIOUS_LINK);
            headerValues.add(previous.toString());
        }
        httpHeaders.addAll(HttpHeaders.LINK, headerValues);
        httpHeaders.set(X_TOTAL_COUNT_HEADER, String.valueOf(page.getTotalCount()));
        return new ResponseEntity<>(page.getEntities(), httpHeaders, HttpStatus.OK);
    }

    /**
     * This method is used to search {@link NewsDTO} by several parameters
     * To find news the query must be like this:
     * ?search=authorName:Test01;tags=tag1;
     * <p>
     * This method can find news by author by this keys:
     * <ol>
     *     <li>name - authorName</li>
     *     <li>surname - authorSurname</li>
     * </ol>
     * This method can find news by tags:
     * <ol>
     *     <li>tag - tags</li>
     * </ol>
     *
     * @param search search parameter to find {@link NewsDTO}
     * @return java.util.List of {@link NewsDTO}
     */

    @GetMapping(params = {"search"})
    public ResponseEntity<List<NewsDTO>> getFoundNews(@Pattern(regexp = SEARCH_QUERY_PATTERN,
            message = INCORRECT_QUERY_VALUE, flags = Pattern.Flag.CASE_INSENSITIVE)
                                                      @RequestParam(value = "search") String search) {
        List<SearchCriteriaDTO> searchCriteria = RequestUtil.mapSearchCriteriaFromRequest(search,
                getSearchCriteriaMapper());
        List<SortCriteriaDTO> sortCriteria = new ArrayList<>();
        HttpHeaders httpHeaders = new HttpHeaders();
        List<NewsDTO> news = newsService.findByCriteria(searchCriteria, sortCriteria);
        httpHeaders.add(X_TOTAL_COUNT_HEADER, String.valueOf(news.size()));
        return new ResponseEntity<>(news, httpHeaders, HttpStatus.OK);
    }

    /**
     * This method is used to search {@link NewsDTO} by several parameters and in several parts
     * To find news the query must be like this:
     * ?search=authorName:Test01;tags=tag1;pag=limit:5;page:1;
     * <p>
     * This method can find news by author by this keys:
     * <ol>
     *     <li>name - authorName</li>
     *     <li>surname - authorSurname</li>
     * </ol>
     * This method can find news by tags:
     * <ol>
     *     <li>tag - tags</li>
     * </ol>
     *
     * @param pag    pagination parameter, includes two sub parameters: limit - how many {@link NewsDTO} must be in
     *               the response, page - what part of java.util.List {@link NewsDTO} must be returned.
     *               Example: ?pag=limit:5;page:1;
     * @param search search parameter to find {@link NewsDTO}
     * @return java.util.List of {@link NewsDTO}
     */

    @GetMapping(params = {"search", "pag"})
    public ResponseEntity<List<NewsDTO>> getFoundNews(@Pattern(regexp = PAGINATION_QUERY_PATTERN,
            message = INCORRECT_QUERY_VALUE)
                                                      @RequestParam(value = "pag") String pag,
                                                      @Pattern(regexp = SEARCH_QUERY_PATTERN,
                                                              message = INCORRECT_QUERY_VALUE,
                                                              flags = Pattern.Flag.CASE_INSENSITIVE)
                                                      @RequestParam(value = "search") String search) {
        List<SearchCriteriaDTO> searchCriteria = RequestUtil.mapSearchCriteriaFromRequest(search,
                getSearchCriteriaMapper());
        List<SortCriteriaDTO> sortCriteria = new ArrayList<>();
        Pagination pagination = RequestUtil.mapPaginationFromRequest(pag);
        Page<NewsDTO> page = newsService.findByCriteria(searchCriteria, sortCriteria, pagination);
        HttpHeaders httpHeaders = new HttpHeaders();
        List<String> headerValues = new ArrayList<>();
        if (Objects.nonNull(page.getNextPage())) {
            Link next = linkTo(methodOn(NewsController.class).getFoundNews(page.getNextPage(), search))
                    .withRel(NEXT_LINK);
            headerValues.add(next.toString());
        }
        if (Objects.nonNull(page.getLastPage())) {
            Link last = linkTo(methodOn(NewsController.class).getFoundNews(page.getLastPage(), search))
                    .withRel(LATS_LINK);
            headerValues.add(last.toString());
        }
        if (Objects.nonNull(page.getPreviousPage())) {
            Link previous = linkTo(methodOn(NewsController.class).getFoundNews(page.getPreviousPage(), search))
                    .withRel(PREVIOUS_LINK);
            headerValues.add(previous.toString());
        }
        httpHeaders.addAll(HttpHeaders.LINK, headerValues);
        httpHeaders.set(X_TOTAL_COUNT_HEADER, String.valueOf(page.getTotalCount()));
        return new ResponseEntity<>(page.getEntities(), httpHeaders, HttpStatus.OK);
    }


    /**
     * This method is used to search {@link NewsDTO} and sort result.
     * To find news the query must be like this:
     * ?search=authorName:Test01;tags=tag1;&sort=authorName:asc;creationDate:desc;
     * <p>
     * This method can find news by author by this keys:
     * <ol>
     *     <li>name - authorName</li>
     *     <li>surname - authorSurname</li>
     * </ol>
     * This method can find news by tags:
     * <ol>
     *     <li>tag - tags</li>
     * </ol>
     * If method find some {@link NewsDTO} and if sort parameter exists, can sort news.
     * Sort parameters must be like this: sort=creationDate:asc;authorName:desc;
     * <p>
     *     This method can sort by author name and/or surname, by creation date, modification date
     *
     * @param search search parameter to find {@link NewsDTO}
     * @param sort   parameter for sort {@link NewsDTO}
     * @return java.util.List of {@link NewsDTO}
     */

    @GetMapping(params = {"search", "sort"})
    public ResponseEntity<List<NewsDTO>> getFoundSortedNews(@Pattern(regexp =
            SEARCH_QUERY_PATTERN, message = INCORRECT_QUERY_VALUE)
                                                            @RequestParam(value = "search") String search,
                                                            @Pattern(regexp = SORT_QUERY_PATTERN,
                                                                    message = INCORRECT_QUERY_VALUE,
                                                                    flags = Pattern.Flag.CASE_INSENSITIVE)
                                                            @RequestParam(value = "sort") String sort) {
        List<SearchCriteriaDTO> searchCriteria = RequestUtil.mapSearchCriteriaFromRequest(search,
                getSearchCriteriaMapper());
        List<SortCriteriaDTO> sortCriteria = RequestUtil.mapSortParametersFromRequest(sort, getSortCriteriaMapper());
        List<NewsDTO> news = newsService.findByCriteria(searchCriteria, sortCriteria);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(X_TOTAL_COUNT_HEADER, String.valueOf(news.size()));
        return new ResponseEntity<>(news, httpHeaders, HttpStatus.OK);
    }


    /**
     * This method is used to search {@link NewsDTO} and sort result and get them in several parts.
     * To find news the query must be like this:
     * ?search=authorName:Test01;tags=tag1;&sort=authorName:asc;creationDate:desc;&pag=limit:5;page:1;
     * <p>
     * This method can find news by author by this keys:
     * <ol>
     *     <li>name - authorName</li>
     *     <li>surname - authorSurname</li>
     * </ol>
     * This method can find news by tags:
     * <ol>
     *     <li>tag - tags</li>
     * </ol>
     * If method find some {@link NewsDTO} and if sort parameter exists, can sort news.
     * Sort parameters must be like this: sort=creationDate:asc;authorName:desc;
     * <p>
     *     This method can sort by author name and/or surname, by creation date, modification date
     *
     * @param search search parameter to find {@link NewsDTO}
     * @param sort   parameter for sort {@link NewsDTO}
     * @param pag    pagination parameter, includes two sub parameters: limit - how many {@link NewsDTO} must be in
     *               the response, page - what part of java.util.List {@link NewsDTO} must be returned.
     *               Example: ?pag=limit:5;page:1;
     * @return java.util.List of {@link NewsDTO}
     */

    @GetMapping(params = {"search", "sort", "pag"})
    public ResponseEntity<List<NewsDTO>> getFoundSortedNews(@Pattern(regexp =
            SEARCH_QUERY_PATTERN, message = INCORRECT_QUERY_VALUE)
                                                            @RequestParam(value = "search") String search,
                                                            @Pattern(regexp = SORT_QUERY_PATTERN,
                                                                    message = INCORRECT_QUERY_VALUE)
                                                            @RequestParam(value = "sort") String sort,
                                                            @Pattern(regexp = PAGINATION_QUERY_PATTERN,
                                                                    message = INCORRECT_QUERY_VALUE,
                                                                    flags = Pattern.Flag.CASE_INSENSITIVE)
                                                            @RequestParam(value = "pag") String pag) {
        List<SearchCriteriaDTO> searchCriteria = RequestUtil.mapSearchCriteriaFromRequest(search,
                getSearchCriteriaMapper());
        List<SortCriteriaDTO> sortCriteria = RequestUtil.mapSortParametersFromRequest(sort, getSortCriteriaMapper());
        Pagination pagination = RequestUtil.mapPaginationFromRequest(pag);
        Page<NewsDTO> page = newsService.findByCriteria(searchCriteria, sortCriteria, pagination);
        HttpHeaders httpHeaders = new HttpHeaders();
        List<String> headerValues = new ArrayList<>();
        if (Objects.nonNull(page.getNextPage())) {
            Link next = linkTo(methodOn(NewsController.class).getFoundSortedNews(search, sort, page.getNextPage()))
                    .withRel(NEXT_LINK);
            headerValues.add(next.toString());
        }
        if (Objects.nonNull(page.getLastPage())) {
            Link last = linkTo(methodOn(NewsController.class).getFoundSortedNews(search, sort, page.getLastPage()))
                    .withRel(LATS_LINK);
            headerValues.add(last.toString());
        }
        if (Objects.nonNull(page.getPreviousPage())) {
            Link previous = linkTo(methodOn(NewsController.class).getFoundSortedNews(search, sort,
                    page.getPreviousPage()))
                    .withRel(PREVIOUS_LINK);
            headerValues.add(previous.toString());
        }
        httpHeaders.addAll(HttpHeaders.LINK, headerValues);
        httpHeaders.set(X_TOTAL_COUNT_HEADER, String.valueOf(page.getTotalCount()));
        return new ResponseEntity<>(page.getEntities(), httpHeaders, HttpStatus.OK);
    }

    /**
     * This method is used to get count of {@link NewsDTO} in database.
     *
     * @return return count of {@link NewsDTO}
     */
    @GetMapping(value = "/count")
    public long getNewsCount() {
        return newsService.countNews();
    }

    /**
     * This method is used to get {@link SearchCriteriaMapper} for mapping incoming request param to
     * {@link SearchCriteriaDTO}
     *
     * @return {@link SearchCriteriaMapper}
     */

    private SearchCriteriaMapper getSearchCriteriaMapper() {
        return searchMappers.get(NEWS_SEARCH_CRITERIA_MAPPER);
    }

    /**
     * This method is used to get {@link SortCriteriaMapper} for mapping incoming request param to
     * {@link SortCriteriaDTO}
     *
     * @return {@link SortCriteriaMapper}
     */

    private SortCriteriaMapper getSortCriteriaMapper() {
        return sortMappers.get(NEWS_SORT_CRITERIA_MAPPER);
    }
}
