package com.epam.lab.exception;

public class CriteriaNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1919798715958782307L;

    public CriteriaNotFoundException(String message) {
        super(message);
    }
}
