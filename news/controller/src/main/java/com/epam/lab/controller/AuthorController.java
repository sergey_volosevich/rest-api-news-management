package com.epam.lab.controller;


import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.service.AuthorService;
import com.epam.lab.transfer.AuthorTransfer;
import com.epam.lab.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.Pattern;
import java.net.URI;
import java.text.MessageFormat;
import java.util.List;


/**
 * This class is used for working with {@link AuthorDTO}. Create, read, update and delete method are represented
 * in this class.
 */

@RestController
@RequestMapping("/authors")
@Validated
public class AuthorController {

    private static final String NUMBER_PATTERN = "^[1-9]\\d*$";
    private static final String AUTHOR_NOT_FOUND = "Author not found";
    private static final String VALIDATION_MSG_FOR_ID = "Invalid request";
    private static final String FAILED_TO_UPDATE_AUTHOR = "Failed to update author. Caused: can not find author with"
            + " id = {0}";
    private static final String FAILED_TO_DELETE_AUTHOR = "Failed to delete author. Caused: can not find author with"
            + " id = {0}";
    private static final String PART_PATH_FOR_LOCATION_HEADER = "authors";

    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }


    /**
     * This method gets and displayed {@link AuthorDTO}
     *
     * @param id unique identifier of {@link AuthorDTO}
     * @return {@link AuthorDTO} representation or throw {@link com.epam.lab.exception.EntityNotFoundException} if
     * does not exist.
     */

    @GetMapping(value = "/{id}")
    public AuthorDTO getAuthor(@PathVariable("id") @Pattern(regexp = NUMBER_PATTERN,
            message = VALIDATION_MSG_FOR_ID, flags = Pattern.Flag.CASE_INSENSITIVE) String id) {
        try {
            return authorService.getById(Long.parseLong(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, AUTHOR_NOT_FOUND, e);
        }
    }

    /**
     * This method is used to get all {@link AuthorDTO}
     *
     * @return java.util.List of {@link AuthorDTO}
     */

    @GetMapping
    public List<AuthorDTO> getAllAuthors() {
        return authorService.getAll();
    }

    /**
     * This method is used to create {@link AuthorDTO}
     *
     * @param authorDTO contains all information to save
     * @param ucb       {@link UriComponentsBuilder} need to build URI for Header Location.
     * @return {@link AuthorDTO} representation
     */

    @PostMapping()
    public ResponseEntity<AuthorDTO> saveAuthor(@Validated(AuthorTransfer.New.class)
                                                @RequestBody AuthorDTO authorDTO, UriComponentsBuilder ucb) {
        AuthorDTO savedAuthor = authorService.save(authorDTO);
        long id = savedAuthor.getId();
        URI locationURL = RequestUtil.buildLocationURL(ucb, PART_PATH_FOR_LOCATION_HEADER, String.valueOf(id));
        HttpHeaders headers = new HttpHeaders();
        RequestUtil.addLocationUrlToHeader(headers, locationURL);
        return new ResponseEntity<>(savedAuthor, headers, HttpStatus.CREATED);
    }

    /**
     * This method is used to update concrete {@link AuthorDTO}.
     *
     * @param id        unique identifier of {@link AuthorDTO}
     * @param authorDTO contains all information to update
     * @param ucb{@link UriComponentsBuilder} need to build URI for Header Location.
     * @return {@link AuthorDTO} representation or throw {@link com.epam.lab.exception.EntityNotFoundException} if
     * does not exist.
     */

    @PutMapping(value = "/{id}")
    public ResponseEntity<AuthorDTO> updateAuthor(@PathVariable @Pattern(regexp = NUMBER_PATTERN,
            message = VALIDATION_MSG_FOR_ID, flags = Pattern.Flag.CASE_INSENSITIVE) String id,
                                                  @Validated(AuthorTransfer.UpdateAuthor.class)
                                                  @RequestBody AuthorDTO authorDTO, UriComponentsBuilder ucb) {
        try {
            AuthorDTO updatedAuthor = authorService.update(authorDTO);
            return new ResponseEntity<>(updatedAuthor, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            String msg = MessageFormat.format(FAILED_TO_UPDATE_AUTHOR, id);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg, e);
        }
    }

    /**
     * This method is used to delete concrete {@link AuthorDTO}
     *
     * @param id unique identifier of {@link AuthorDTO}
     */

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeAuthor(@PathVariable("id") @Pattern(regexp = NUMBER_PATTERN,
            message = VALIDATION_MSG_FOR_ID, flags = Pattern.Flag.CASE_INSENSITIVE) String id) {
        try {
            authorService.delete(Long.parseLong(id));
        } catch (EntityNotFoundException e) {
            String msg = MessageFormat.format(FAILED_TO_DELETE_AUTHOR, id);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg, e);
        }
    }
}
