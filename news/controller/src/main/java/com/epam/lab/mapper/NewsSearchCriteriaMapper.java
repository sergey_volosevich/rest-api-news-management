package com.epam.lab.mapper;

import com.epam.lab.dto.SearchCriteriaDTO;
import com.epam.lab.exception.CriteriaNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class NewsSearchCriteriaMapper implements SearchCriteriaMapper {

    private static final Logger LOGGER = LogManager.getLogger(NewsSearchCriteriaMapper.class);

    private static final String FAILED_TO_PARSE_QUERY = "Failed to parse query";
    private static final String PROVIDE_VALID_PARAMETERS_TO_SEARCH = "Provide valid parameters for search. Can not find"
            + " search parameter with name {0}";
    private static final String INVALID_DATE_FORMAT = "Invalid date format for parameter {0}";
    private static final String SEARCH_PARAMETER_PATTERN = "(\\w+?)([:><])([A-Za-z0-9-_]+);";
    private static final String CREATION_DATE_FIELD = "creationDate";
    private static final String MODIFICATION_DATE_FIELD = "modificationDate";
    private static final String DATE_FORMAT_PATTERN = "([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))";
    private static final String INVALID_REQUEST_PARAMETERS = "Provide valid parameters for search. " +
            "Invalid parameters syntax.";

    @Override
    public List<SearchCriteriaDTO> mapToSearchCriteria(String query) {
        List<SearchCriteriaDTO> criteriaList = new ArrayList<>();
        Matcher matcher = getMatcher(query, SEARCH_PARAMETER_PATTERN);
        while (matcher.find()) {
            String columnName = matcher.group(1);
            String operation = matcher.group(2);
            String value = matcher.group(3);
            NewsFieldName newsFieldName = getNewsFieldName(columnName);
            String searchParameter = newsFieldName.getNameOfClassField();
            if (searchParameter.equalsIgnoreCase(CREATION_DATE_FIELD) ||
                    searchParameter.equalsIgnoreCase(MODIFICATION_DATE_FIELD)) {
                Matcher matcherDate = getMatcher(value, DATE_FORMAT_PATTERN);
                if (matcherDate.matches()) {
                    criteriaList.add(new SearchCriteriaDTO(searchParameter, operation, value));
                } else {
                    LOGGER.error(FAILED_TO_PARSE_QUERY);
                    throw new CriteriaNotFoundException(MessageFormat.format(INVALID_DATE_FORMAT, searchParameter));
                }
            }
            criteriaList.add(new SearchCriteriaDTO(searchParameter, operation, value));
        }
        if (criteriaList.isEmpty()) {
            throw new CriteriaNotFoundException(INVALID_REQUEST_PARAMETERS);
        }
        return criteriaList;
    }

    private NewsFieldName getNewsFieldName(String columnName) {
        Optional<NewsFieldName> columnNameOptional = NewsFieldName.getColumnNameFromString(columnName);
        if (!columnNameOptional.isPresent()) {
            LOGGER.error(FAILED_TO_PARSE_QUERY);
            throw new CriteriaNotFoundException(MessageFormat.format(PROVIDE_VALID_PARAMETERS_TO_SEARCH, columnName));
        }
        return columnNameOptional.get();
    }

    private static Matcher getMatcher(String queryString, String patternString) {
        Pattern pattern = Pattern.compile(patternString);
        return pattern.matcher(queryString);
    }
}
