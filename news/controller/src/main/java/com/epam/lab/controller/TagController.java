package com.epam.lab.controller;

import com.epam.lab.dto.TagDTO;
import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.service.TagService;
import com.epam.lab.transfer.TagTransfer;
import com.epam.lab.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.Pattern;
import java.net.URI;
import java.text.MessageFormat;
import java.util.List;


/**
 * This class is used for working with {@link TagDTO}. Create, read, update and delete method are represented
 * in this class.
 */


@RestController
@RequestMapping(value = "/tags")
@Validated
public class TagController {

    private static final String TAG_NOT_FOUND = "Tag not found";
    private static final String NUMBER_PATTERN = "^[1-9]\\d*$";
    private static final String FAILED_TO_UPDATE_TAG = "Failed to update tag. Caused: can not find tag with id = {0}";
    private static final String FAILED_TO_DELETE_TAG = "Failed to delete tag. Caused: can not find tag with id = {0}";
    private static final String VALIDATION_MSG_FOR_ID = "Invalid request";
    private static final String PART_PATH_FOR_LOCATION_HEADER = "tags";

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }


    /**
     * This method gets and displayed {@link TagDTO}
     *
     * @param id unique identifier of {@link TagDTO}
     * @return {@link TagDTO} representation or throw {@link com.epam.lab.exception.EntityNotFoundException} if
     * does not exist.
     */

    @GetMapping(value = "/{id}")
    public TagDTO getTag(@PathVariable @Pattern(regexp = NUMBER_PATTERN,
            message = VALIDATION_MSG_FOR_ID, flags = Pattern.Flag.CASE_INSENSITIVE) String id) {
        try {
            return tagService.getById(Long.parseLong(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, TAG_NOT_FOUND, e);
        }
    }

    /**
     * This method is used to get all {@link TagDTO}
     *
     * @return java.util.List of {@link TagDTO}
     */

    @GetMapping
    public List<TagDTO> getAllTags() {
        return tagService.getAll();
    }

    /**
     * This method is used to create {@link TagDTO}
     *
     * @param tagDTO contains all information to save
     * @param ucb    {@link UriComponentsBuilder} need to build URI for Header Location.
     * @return {@link TagDTO} representation
     */

    @PostMapping()
    public ResponseEntity<TagDTO> saveTag(@Validated(TagTransfer.New.class)
                                          @RequestBody TagDTO tagDTO, UriComponentsBuilder ucb) {
        TagDTO savedTag = tagService.save(tagDTO);
        long id = savedTag.getId();
        HttpHeaders headers = new HttpHeaders();
        URI locationURL = RequestUtil.buildLocationURL(ucb, PART_PATH_FOR_LOCATION_HEADER, String.valueOf(id));
        RequestUtil.addLocationUrlToHeader(headers, locationURL);
        return new ResponseEntity<>(savedTag, headers, HttpStatus.CREATED);
    }

    /**
     * This method is used to update concrete {@link TagDTO}.
     *
     * @param id     unique identifier of {@link TagDTO}
     * @param tagDTO contains all information to update
     * @param ucb    {@link UriComponentsBuilder} need to build URI for Header Location.
     * @return {@link TagDTO} representation or throw {@link com.epam.lab.exception.EntityNotFoundException} if
     * does not exist.
     */

    @PutMapping(value = "/{id}")
    public ResponseEntity<TagDTO> updateTag(@PathVariable @Pattern(regexp = NUMBER_PATTERN,
            message = VALIDATION_MSG_FOR_ID, flags = Pattern.Flag.CASE_INSENSITIVE) String id,
                                            @Validated(TagTransfer.UpdateTag.class)
                                            @RequestBody TagDTO tagDTO, UriComponentsBuilder ucb) {
        try {
            TagDTO updatedTag = tagService.update(tagDTO);
            return new ResponseEntity<>(updatedTag, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            String msg = MessageFormat.format(FAILED_TO_UPDATE_TAG, id);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg, e);
        }
    }

    /**
     * This method is used to delete concrete {@link TagDTO}
     *
     * @param id unique identifier of {@link TagDTO}
     */

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeTag(@PathVariable("id") @Pattern(regexp = NUMBER_PATTERN,
            message = VALIDATION_MSG_FOR_ID, flags = Pattern.Flag.CASE_INSENSITIVE) String id) {
        try {
            tagService.delete(Long.parseLong(id));
        } catch (EntityNotFoundException e) {
            String msg = MessageFormat.format(FAILED_TO_DELETE_TAG, id);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg, e);
        }
    }
}
