package com.epam.lab.cofiguration;

import com.epam.lab.configuration.PersistenceContextConfig;
import com.epam.lab.configuration.ServiceContextConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class MainWebInitializer implements WebApplicationInitializer {

    private static final String SPRING_PROFILES_ACTIVE = "spring.profiles.active";
    private static final String ENCODING_FILTER_NAME = "encodingFilter";
    private static final String PRODUCTION_PROFILE = "prod";
    private static final String ENCODING = "UTF-8";

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext root =
                new AnnotationConfigWebApplicationContext();
        root.register(ControllerContextConfig.class, ServiceContextConfig.class, PersistenceContextConfig.class);
        servletContext.setInitParameter(SPRING_PROFILES_ACTIVE, PRODUCTION_PROFILE);
        CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter(ENCODING, true);
        FilterRegistration.Dynamic filterRegistration =
                servletContext.addFilter(ENCODING_FILTER_NAME, encodingFilter);
        filterRegistration.addMappingForUrlPatterns(null, false, "/*");
        root.setServletContext(servletContext);
        root.refresh();
        ServletRegistration.Dynamic appServlet =
                servletContext.addServlet("news", new DispatcherServlet(root));
        appServlet.setLoadOnStartup(1);
        appServlet.addMapping("/");
    }
}
