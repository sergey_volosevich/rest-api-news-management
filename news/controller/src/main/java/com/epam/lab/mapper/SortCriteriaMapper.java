package com.epam.lab.mapper;

import com.epam.lab.dto.SortCriteriaDTO;

import java.util.List;

public interface SortCriteriaMapper {
    List<SortCriteriaDTO> mapToSortCriteria(String query);
}
