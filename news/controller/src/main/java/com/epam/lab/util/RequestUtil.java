package com.epam.lab.util;

import com.epam.lab.dto.Pagination;
import com.epam.lab.dto.SearchCriteriaDTO;
import com.epam.lab.dto.SortCriteriaDTO;
import com.epam.lab.mapper.SearchCriteriaMapper;
import com.epam.lab.mapper.SortCriteriaMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class RequestUtil {

    private static final String PAGINATION_PARAMETER_PATTERN = "(limit|page)([:])([1-9]\\d*)";
    private static final String LIMIT_FILED = "limit";

    private RequestUtil() {
    }

    public static List<SearchCriteriaDTO> mapSearchCriteriaFromRequest(String query, SearchCriteriaMapper mapper) {
        return mapper.mapToSearchCriteria(query);
    }

    public static URI buildLocationURL(UriComponentsBuilder ucb, String partPath, String locationPartPath) {
        return ucb.path("/")
                .path(partPath)
                .path("/")
                .path(locationPartPath)
                .build()
                .toUri();
    }

    public static void addLocationUrlToHeader(HttpHeaders headers, URI locationURL) {
        headers.setLocation(locationURL);
    }

    public static List<SortCriteriaDTO> mapSortParametersFromRequest(String query, SortCriteriaMapper mapper) {
        return mapper.mapToSortCriteria(query);
    }

    public static Pagination mapPaginationFromRequest(String pagination) {
        Pagination mappedPagination = new Pagination();
        Matcher matcher = getMatcher(pagination);
        while (matcher.find()) {
            String parameterName = matcher.group(1);
            String parameterValue = matcher.group(3);
            if (parameterName.equals(LIMIT_FILED)) {
                mappedPagination.setLimit(Integer.parseInt(parameterValue));

            } else {
                mappedPagination.setPage(Integer.parseInt(parameterValue));
            }
        }
        return mappedPagination;
    }

    private static Matcher getMatcher(String queryString) {
        Pattern pattern = Pattern.compile(RequestUtil.PAGINATION_PARAMETER_PATTERN);
        return pattern.matcher(queryString);
    }
}
