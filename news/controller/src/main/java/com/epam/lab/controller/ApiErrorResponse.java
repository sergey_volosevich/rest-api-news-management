package com.epam.lab.controller;

import org.springframework.http.HttpStatus;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class ApiErrorResponse {

    private Map<String, Object> responseBody;

    public Map<String, Object> getResponseBody() {
        return responseBody;
    }

    public static final class ResponseBodyBuilder {
        private static final String TIMESTAMP = "timestamp";
        private static final String STATUS = "status";
        private static final String ERROR_CODE = "error code";
        private static final String MESSAGE = "message";
        private static final String MESSAGES = "messages";

        private HttpStatus httpStatus;
        private int errorCode;
        private String validationMessage;
        private String errorTimeStamp;
        private List<String> validationMessages;


        ResponseBodyBuilder() {
        }

        public ResponseBodyBuilder withStatus(HttpStatus status) {
            this.httpStatus = status;
            return this;
        }

        public ResponseBodyBuilder withErrorCode(int errorCode) {
            this.errorCode = errorCode;
            return this;
        }

        public ResponseBodyBuilder withMessage(String message) {
            this.validationMessage = message;
            return this;
        }

        public ResponseBodyBuilder withMessage(List<String> messages) {
            this.validationMessages = messages;
            return this;
        }

        public ResponseBodyBuilder atTime(String timeStamp) {
            this.errorTimeStamp = timeStamp;
            return this;
        }

        public ApiErrorResponse build() {
            ApiErrorResponse apiErrorResponse = new ApiErrorResponse();
            Map<String, Object> body = new LinkedHashMap<>();
            body.put(TIMESTAMP, this.errorTimeStamp);
            body.put(STATUS, this.httpStatus);
            body.put(ERROR_CODE, this.errorCode);
            if (this.validationMessage == null) {
                body.put(MESSAGES, this.validationMessages);
            } else {
                body.put(MESSAGE, this.validationMessage);
            }
            apiErrorResponse.responseBody = body;
            return apiErrorResponse;
        }
    }
}
