package com.epam.lab.builder;

import com.epam.lab.dto.TagDTO;

public class TagBuilder {
    private Long id;
    private String name;

    public TagBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public TagBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public TagDTO build() {
        return new TagDTO(id, name);
    }
}
