package com.epam.lab.controller;

import com.epam.lab.builder.TagBuilder;
import com.epam.lab.dto.TagDTO;
import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.service.TagService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestContext.class})
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TagControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private TagService tagService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetTag_TagFound_ShouldReturnTag() throws Exception {
        TagDTO tagDTO = new TagBuilder()
                .withId(1L)
                .withName("Tag-test01")
                .build();
        when(tagService.getById(1L)).thenReturn(tagDTO);
        mockMvc.perform(get("/tags/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Tag-test01")));

        verify(tagService, times(1)).getById(1L);
        verifyNoMoreInteractions(tagService);
    }

    @Test
    public void testGetTag_EntityNotFound_ShouldReturnHttpStatusCode404() throws Exception {
        when(tagService.getById(1L)).thenThrow(new EntityNotFoundException());

        mockMvc.perform(get("/tags/{id}", 1L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("NOT_FOUND")))
                .andExpect(jsonPath("$.['error code']", is(404)))
                .andExpect(jsonPath("$.message", is("Tag not found")));
        verify(tagService, times(1)).getById(1L);
        verifyNoMoreInteractions(tagService);
    }

    @Test
    public void testGetTag_InvalidPathVariable_ShouldReturnHttpStatusCode400() throws Exception {
        mockMvc.perform(get("/tags/{id}", "notNumber"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages.[0]", is("Invalid request")));
        verifyNoInteractions(tagService);
    }

    @Test
    public void testSaveTag_ValidRequestBody_ShouldReturnSavedTag() throws Exception {
        TagDTO inputTagDTO = new TagBuilder()
                .withName("Tag_test01")
                .build();
        TagDTO outputTagDTO = new TagBuilder()
                .withId(1L)
                .withName("Tag_test01")
                .build();
        when(tagService.save(inputTagDTO)).thenReturn(outputTagDTO);

        mockMvc.perform(post("/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(inputTagDTO)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(header().string(HttpHeaders.LOCATION, "http://localhost/tags/1"))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Tag_test01")));

        ArgumentCaptor<TagDTO> captor = ArgumentCaptor.forClass(TagDTO.class);
        verify(tagService, times(1)).save(captor.capture());
        verifyNoMoreInteractions(tagService);

        TagDTO tagDTO = captor.getValue();
        assertNull(tagDTO.getId());
        assertEquals("Tag_test01", tagDTO.getName());
    }

    @Test
    public void testSaveTag_InvalidRequestBody_ShouldReturnHttpStatus400() throws Exception {
        TagDTO tagDTO = new TagBuilder()
                .build();
        mockMvc.perform(post("/tags")
                .content(objectMapper.writeValueAsBytes(tagDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages.[*]", containsInAnyOrder("Please provide a tag")));

        verifyNoInteractions(tagService);
    }

    @Test
    public void testSaveTag_InvalidJson_ShouldReturnHttpStatus400() throws Exception {
        String json = "{\n" +
                "\t\"id\":\"not a number\",\n" +
                "\t\"name\":\"Tag_test01\",\n" +
                "}";
        mockMvc.perform(post("/tags")
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Malformed JSON request")));
        verifyNoInteractions(tagService);
    }


    @Test
    public void testUpdateTag_ValidRequestBody_ShouldReturnUpdatedTag() throws Exception {
        TagDTO inputTagDTO = new TagBuilder()
                .withId(1L)
                .withName("Tag_test01")
                .build();

        when(tagService.update(inputTagDTO)).thenReturn(inputTagDTO);

        mockMvc.perform(put("/tags/{id}", 1L)
                .content(objectMapper.writeValueAsBytes(inputTagDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Tag_test01")));
        ArgumentCaptor<TagDTO> captor = ArgumentCaptor.forClass(TagDTO.class);
        verify(tagService, times(1)).update(captor.capture());
        verifyNoMoreInteractions(tagService);

        TagDTO tagDTO = captor.getValue();
        assertEquals(1, tagDTO.getId().longValue());
        assertEquals("Tag_test01", tagDTO.getName());
    }


    @Test
    public void testUpdateTag_InvalidRequestBody_ShouldReturnHttpStatus400() throws Exception {
        TagDTO inputTagDTO = new TagBuilder()
                .withId(1L)
                .withName("Nm")
                .build();
        mockMvc.perform(put("/tags/{id}", 1L)
                .content(objectMapper.writeValueAsBytes(inputTagDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages.[*]", containsInAnyOrder("Please provide a correct "
                        + "value for tag")));
        verifyNoInteractions(tagService);
    }

    @Test
    public void testUpdateTag_TagNotExist_ShouldReturnHttpStatus400() throws Exception {
        TagDTO inputTagDTO = new TagBuilder()
                .withId(1L)
                .withName("Tag_test01")
                .build();
        when(tagService.update(inputTagDTO)).thenThrow(new EntityNotFoundException());
        mockMvc.perform(put("/tags/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(inputTagDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Failed to update tag. Caused: can not find tag"
                        + " with id = 1")));
        verify(tagService, times(1)).update(inputTagDTO);
        verifyNoMoreInteractions(tagService);
    }

    @Test
    public void testRemoveTag_TagExist_ShouldReturnHttpStatus204() throws Exception {
        long tagId = 1;
        doNothing().when(tagService).delete(tagId);

        mockMvc.perform(delete("/tags/{id}", tagId))
                .andExpect(status().isNoContent());
        verify(tagService, times(1)).delete(tagId);
        verifyNoMoreInteractions(tagService);
    }

    @Test
    public void testRemoveTag_TagNotExist_ShouldReturnHttpStatus400() throws Exception {
        long tagId = 1;
        doThrow(new EntityNotFoundException()).when(tagService).delete(tagId);
        mockMvc.perform(delete("/tags/{id}", tagId))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Failed to delete tag. Caused: can not find"
                        + " tag with id = 1")));
        verify(tagService, times(1)).delete(tagId);
        verifyNoMoreInteractions(tagService);
    }

    @Test
    public void testGetAllTags_ShouldReturnFoundTags() throws Exception {
        TagDTO tag1 = new TagBuilder()
                .withId(1L)
                .withName("Tag_test01")
                .build();
        TagDTO tag2 = new TagBuilder()
                .withId(2L)
                .withName("Tag_test02")
                .build();
        List<TagDTO> tags = new ArrayList<>();
        tags.add(tag1);
        tags.add(tag2);
        when(tagService.getAll()).thenReturn(tags);
        mockMvc.perform(get("/tags"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)));
        verify(tagService, times(1)).getAll();
        verifyNoMoreInteractions(tagService);
    }
}