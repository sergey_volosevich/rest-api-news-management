package com.epam.lab.controller;

import com.epam.lab.builder.AuthorBuilder;
import com.epam.lab.builder.NewsBuilder;
import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.Page;
import com.epam.lab.dto.Pagination;
import com.epam.lab.dto.TagDTO;
import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.exception.ResourceNotFoundException;
import com.epam.lab.exception.SearchParameterNotFoundException;
import com.epam.lab.service.NewsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class})
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class NewsControllerTest {

    private static final String PAGINATION_PARAMETER_TEMPLATE = "limit:{0};page:{1};";
    private static final String X_TOTAL_COUNT_HEADER = "X-Total-Count";

    private MockMvc mockMvc;

    @Autowired
    private NewsService newsService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetNews_ValidNewsDTO_ShouldPass() throws Exception {
        NewsDTO newsDTO = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01")
                .withFullText("Some text for news test 01")
                .withCreationDate(LocalDate.of(2020, 2, 14))
                .withModificationDate(LocalDate.of(2020, 2, 14))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag-test01"))
                .withTag(new TagDTO(2L, "tag-test02"))
                .build();
        when(newsService.getById(anyLong())).thenReturn(newsDTO);
        mockMvc.perform(get("/news/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("Title-test-01")))
                .andExpect(jsonPath("$.shortText", is("Short text test01")))
                .andExpect(jsonPath("$.fullText", is("Some text for news test 01")))
                .andExpect(jsonPath("$.creationDate", is("2020-02-14")))
                .andExpect(jsonPath("$.modificationDate", is("2020-02-14")))
                .andExpect(jsonPath("$.author.name", is("Name-test01")))
                .andExpect(jsonPath("$.author.surname", is("Surname-test01")))
                .andExpect(jsonPath("$.tags.[0].name", is("tag-test01")))
                .andExpect(jsonPath("$.tags.[1].name", is("tag-test02")));
        verify(newsService, times(1)).getById(1L);
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetNews_InvalidPathVariable_ShouldReturnHttpStatusCode400() throws Exception {
        mockMvc.perform(get("/news/{id}", "not a number"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages.[0]", is("Invalid request")));
        verifyNoInteractions(newsService);
    }

    @Test
    public void testGetNews_EntityNotFound_ShouldReturnHttpStatusCode404() throws Exception {
        when(newsService.getById(anyLong())).thenThrow(new EntityNotFoundException());

        mockMvc.perform(get("/news/{id}", 2L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("NOT_FOUND")))
                .andExpect(jsonPath("$.['error code']", is(404)))
                .andExpect(jsonPath("$.message", is("News not found")));
        verify(newsService, times(1)).getById(2L);
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testSaveNews_ValidRequestBody_ShouldReturnSavedNews() throws Exception {
        LocalDate now = LocalDate.now();
        NewsDTO inputNewsDTO = new NewsBuilder()
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(now)
                .withModificationDate(now)
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test")
                        .withSurname("Surname-test")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        NewsDTO outputNewsDTO = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(now)
                .withModificationDate(now)
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test")
                        .withSurname("Surname-test")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        when(newsService.save(inputNewsDTO)).thenReturn(outputNewsDTO);

        mockMvc.perform(post("/news")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(inputNewsDTO)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, "http://localhost/news/1"))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("Title-test-01")))
                .andExpect(jsonPath("$.shortText", is("Short text test01")))
                .andExpect(jsonPath("$.fullText", is("Some text for news test 01, Some text for news"
                        + " test 01")))
                .andExpect(jsonPath("$.creationDate", is(now.toString())))
                .andExpect(jsonPath("$.modificationDate", is(now.toString())))
                .andExpect(jsonPath("$.author.name", is("Name-test")))
                .andExpect(jsonPath("$.author.surname", is("Surname-test")))
                .andExpect(jsonPath("$.tags.[0].name", is("tag_test01")))
                .andExpect(jsonPath("$.tags.[1].name", is("tag_test02")));
        ArgumentCaptor<NewsDTO> captor = ArgumentCaptor.forClass(NewsDTO.class);
        verify(newsService, times(1)).save(captor.capture());
        verifyNoMoreInteractions(newsService);
        NewsDTO captureNews = captor.getValue();
        assertNull(captureNews.getId());
    }

    @Test
    public void testSaveNews_InvalidRequestBody_ShouldReturnHttpStatus400() throws Exception {
        LocalDate now = LocalDate.now();
        NewsDTO inputNewsDTO = new NewsBuilder()
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test")
                        .withSurname("Surname-test")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        mockMvc.perform(post("/news")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(inputNewsDTO)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.messages", hasSize(5)))
                .andExpect(jsonPath("$.messages.[*]", containsInAnyOrder("Please provide a title of"
                                + " news", "Please provide a short text of news", "Please provide a full text of news",
                        "Please provide a modification date of news", "Please provide a creation date of news")));
        verifyNoInteractions(newsService);
    }

    @Test
    public void testUpdateNews_NewsExist_ShouldReturnUpdatedNews() throws Exception {
        LocalDate now = LocalDate.now();
        NewsDTO inputNewsDTO = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(now)
                .withModificationDate(now)
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test")
                        .withSurname("Surname-test")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        NewsDTO outputNewsDTO = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(now)
                .withModificationDate(now)
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        when(newsService.update(inputNewsDTO)).thenReturn(outputNewsDTO);
        mockMvc.perform(put("/news/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(inputNewsDTO)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("Title-test-01")))
                .andExpect(jsonPath("$.shortText", is("Short text test01, Short text test01")))
                .andExpect(jsonPath("$.fullText", is("Some text for news test 01, Some text for news"
                        + " test 01")))
                .andExpect(jsonPath("$.creationDate", is(now.toString())))
                .andExpect(jsonPath("$.modificationDate", is(now.toString())))
                .andExpect(jsonPath("$.author.name", is("Name-test01")))
                .andExpect(jsonPath("$.author.surname", is("Surname-test01")))
                .andExpect(jsonPath("$.tags.[0].name", is("tag_test01")))
                .andExpect(jsonPath("$.tags.[1].name", is("tag_test02")));
        ArgumentCaptor<NewsDTO> captor = ArgumentCaptor.forClass(NewsDTO.class);
        verify(newsService, times(1)).update(captor.capture());
        verifyNoMoreInteractions(newsService);
        NewsDTO captureNews = captor.getValue();
        assertEquals(1L, captureNews.getId().longValue());
    }

    @Test
    public void testUpdateNews_NewsNotExist_ShouldReturnHttpStatus400() throws Exception {
        LocalDate now = LocalDate.now();
        NewsDTO inputNewsDTO = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(now)
                .withModificationDate(now)
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test")
                        .withSurname("Surname-test")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        when(newsService.update(inputNewsDTO)).thenThrow(new EntityNotFoundException());
        mockMvc.perform(put("/news/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(inputNewsDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)));
        verify(newsService, times(1)).update(inputNewsDTO);
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testRemoveNews_NewsExist_ShouldHttpStatus204() throws Exception {
        long newsID = 1L;
        doNothing().when(newsService).delete(newsID);
        mockMvc.perform(delete("/news/{id}", newsID))
                .andExpect(status().isNoContent());
        verify(newsService, times(1)).delete(newsID);
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testRemoveNews_NewsNotExist_ShouldReturnHttpStatus400() throws Exception {
        long newsId = 1;
        doThrow(new EntityNotFoundException()).when(newsService).delete(newsId);
        mockMvc.perform(delete("/news/{id}", newsId))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Failed to delete news. Caused: can not find news"
                        + " with id = 1")));
        verify(newsService, times(1)).delete(newsId);
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetNews_ShouldReturnAllNews() throws Exception {
        NewsDTO newsDTO1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        NewsDTO newsDTO2 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        List<NewsDTO> news = new ArrayList<>();
        news.add(newsDTO1);
        news.add(newsDTO2);
        when(newsService.getAll()).thenReturn(news);
        mockMvc.perform(get("/news"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", hasSize(2)));
        verify(newsService, times(1)).getAll();
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetAllNews_WithPaginationParameter_ShouldReturnPartOfNews() throws Exception {
        NewsDTO newsDTO1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        NewsDTO newsDTO2 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        List<NewsDTO> news = new ArrayList<>();
        news.add(newsDTO1);
        news.add(newsDTO2);
        String nextPage = MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, 5, 3);
        String lastPage = MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, 5, 5);
        String previousPage = MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, 5, 1);
        Page<NewsDTO> page = new Page<>(nextPage, previousPage, lastPage, 20, news);
        when(newsService.getNewsInParts(ArgumentMatchers.any(Pagination.class))).thenReturn(page);
        mockMvc.perform(get("/news?pag=limit:5;page:2;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().stringValues(HttpHeaders.LINK,
                        "<http://localhost/news?pag=limit:5;page:3;>;rel=\"next\"",
                        "<http://localhost/news?pag=limit:5;page:5;>;rel=\"last\"",
                        "<http://localhost/news?pag=limit:5;page:1;>;rel=\"previous\""
                ))
                .andExpect(header().string(X_TOTAL_COUNT_HEADER, "20"))
                .andExpect(jsonPath("$.[*]", hasSize(2)));
        verify(newsService, times(1)).getNewsInParts(ArgumentMatchers.any(Pagination.class));
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetAllNews_ThrowException_ShouldReturnHttpStatus404() throws Exception {
        when(newsService.getNewsInParts(any(Pagination.class))).thenThrow(new ResourceNotFoundException("Can not find resource on page 1. Page does not exist."));
        mockMvc.perform(get("/news?pag=limit:5;page:2;"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("NOT_FOUND")))
                .andExpect(jsonPath("$.['error code']", is(404)))
                .andExpect(jsonPath("$.message", is("Can not find resource on page 1. Page does not exist.")));
        verify(newsService, times(1)).getNewsInParts(any(Pagination.class));
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetFoundNews_WithPagination_ShouldReturnPartOfNews() throws Exception {
        NewsDTO newsDTO1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        NewsDTO newsDTO2 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        List<NewsDTO> news = new ArrayList<>();
        news.add(newsDTO1);
        news.add(newsDTO2);
        String nextPage = MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, 5, 3);
        String lastPage = MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, 5, 5);
        String previousPage = MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, 5, 1);
        Page<NewsDTO> page = new Page<>(nextPage, previousPage, lastPage, 20, news);
        when(newsService.findByCriteria(anyList(), anyList(), any(Pagination.class))).thenReturn(page);
        mockMvc.perform(get("/news?search=authorName:Name-test01;&pag=limit:5;page:2;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().stringValues(HttpHeaders.LINK,
                        "<http://localhost/news?pag=limit:5;page:3;&search=authorName:Name-test01;>;"
                                + "rel=\"next\"",
                        "<http://localhost/news?pag=limit:5;page:5;&search=authorName:Name-test01;>;rel=\"last\"",
                        "<http://localhost/news?pag=limit:5;page:1;&search=authorName:Name-test01;>;rel=\"previous\""
                ))
                .andExpect(header().string(X_TOTAL_COUNT_HEADER, "20"))
                .andExpect(jsonPath("$.[*]", hasSize(2)));
        verify(newsService, times(1)).findByCriteria(anyList(), anyList(),
                any(Pagination.class));
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetFoundSortedNews_WithPagination_ShouldReturnPartOfNews() throws Exception {
        NewsDTO newsDTO1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        NewsDTO newsDTO2 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        List<NewsDTO> news = new ArrayList<>();
        news.add(newsDTO1);
        news.add(newsDTO2);
        String nextPage = MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, 5, 3);
        String lastPage = MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, 5, 5);
        String previousPage = MessageFormat.format(PAGINATION_PARAMETER_TEMPLATE, 5, 1);
        Page<NewsDTO> page = new Page<>(nextPage, previousPage, lastPage, 20, news);
        when(newsService.findByCriteria(anyList(), anyList(), any(Pagination.class))).thenReturn(page);
        mockMvc.perform(get("/news?search=authorName:Name-test01;&sort=creationDate:asc;&pag=limit:5;page:2;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().stringValues(HttpHeaders.LINK,
                        "<http://localhost/news?search=authorName:Name-test01;&sort=creationDate:asc;"
                                + "&pag=limit:5;page:3;>;rel=\"next\"",
                        "<http://localhost/news?search=authorName:Name-test01;&sort=creationDate:asc;"
                                + "&pag=limit:5;page:5;>;rel=\"last\"",
                        "<http://localhost/news?search=authorName:Name-test01;&sort=creationDate:asc;"
                                + "&pag=limit:5;page:1;>;rel=\"previous\""
                ))
                .andExpect(header().string(X_TOTAL_COUNT_HEADER, "20"))
                .andExpect(jsonPath("$.[*]", hasSize(2)));
        verify(newsService, times(1)).findByCriteria(anyList(), anyList(),
                any(Pagination.class));
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetNews_SearchParameterExist_ShouldReturnFoundNews() throws Exception {
        NewsDTO newsDTO1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        NewsDTO newsDTO2 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        List<NewsDTO> news = new ArrayList<>();
        news.add(newsDTO1);
        news.add(newsDTO2);
        when(newsService.findByCriteria(anyList(), anyList())).thenReturn(news);
        mockMvc.perform(get("/news?search=authorName:Name-test01;creationDate:2020-02-25;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", hasSize(2)));
        verify(newsService, times(1)).findByCriteria(anyList(), anyList());
        verifyNoMoreInteractions(newsService);
    }


    @Test
    public void testGetNews_InvalidSearchParameterName_ShouldReturnHttpStatus400() throws Exception {
        mockMvc.perform(get("/news?search=author:Name-test01;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Provide valid parameters for search."
                        + " Can not find search parameter with name author")));
    }

    @Test
    public void testGetNews_InvalidSearchParameterValue_ShouldReturnHttpStatus400() throws Exception {
        mockMvc.perform(get("/news?search=tag:test01,test02;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Provide valid parameters for search. " +
                        "Invalid parameters syntax.")));
    }

    @Test
    public void testGetNews_InvalidValueForSearchParameter_ShouldReturnHttpStatus400() throws Exception {
        mockMvc.perform(get("/news?search=creationDate:notNumber;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Invalid date format for parameter creationDate")));
    }

    @Test
    public void testGetNews_SortParameterAscending_ShouldReturnFoundNews() throws Exception {
        NewsDTO newsDTO1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        NewsDTO newsDTO2 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        List<NewsDTO> news = new ArrayList<>();
        news.add(newsDTO1);
        news.add(newsDTO2);
        when(newsService.findByCriteria(anyList(), anyList())).thenReturn(news);
        mockMvc.perform(get("/news?search=authorName:Name-test01;&sort=creationDate:asc;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", hasSize(2)));
        verify(newsService, times(1)).findByCriteria(anyList(), anyList());
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetNews_SortParameterDescending_ShouldReturnFoundNews() throws Exception {
        NewsDTO newsDTO1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        NewsDTO newsDTO2 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        List<NewsDTO> news = new ArrayList<>();
        news.add(newsDTO1);
        news.add(newsDTO2);
        when(newsService.findByCriteria(anyList(), anyList())).thenReturn(news);
        mockMvc.perform(get("/news?search=authorName:Name-test01;&sort=creationDate:desc;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", hasSize(2)));
        verify(newsService, times(1)).findByCriteria(anyList(), anyList());
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetNews_InvalidValueForSortParameter_ShouldReturnHttpStatus400() throws Exception {
        mockMvc.perform(get("/news?search=creationDate:2020-02-10;&sort=creationDate:notCorrectValue;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Provide valid parameters for sort. Invalid value"
                        + " for parameter creationDate")));
    }

    @Test
    public void testGetNews_InvalidSortParameter_ShouldReturnHttpStatus400() throws Exception {
        mockMvc.perform(get("/news?search=creationDate:2020-02-10;&sort=Date:notCorrectValue;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Provide valid parameters for sort. Can not find"
                        + " sort parameter with name Date")));
    }

    @Test
    public void testGetNews_SearchParameterExist_ShouldPass() throws Exception {
        NewsDTO newsDTO1 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        NewsDTO newsDTO2 = new NewsBuilder()
                .withId(1L)
                .withTitle("Title-test-01")
                .withShortText("Short text test01, Short text test01")
                .withFullText("Some text for news test 01, Some text for news test 01")
                .withCreationDate(LocalDate.parse("2020-02-14"))
                .withModificationDate(LocalDate.parse("2020-02-14"))
                .withAuthor(new AuthorBuilder()
                        .withId(1L)
                        .withName("Name-test01")
                        .withSurname("Surname-test01")
                        .build())
                .withTag(new TagDTO(1L, "tag_test01"))
                .withTag(new TagDTO(2L, "tag_test02"))
                .build();
        List<NewsDTO> news = new ArrayList<>();
        news.add(newsDTO1);
        news.add(newsDTO2);
        when(newsService.findByCriteria(anyList(), anyList())).thenReturn(news);
        mockMvc.perform(get("/news?search=modificationDate:2020-02-25;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", hasSize(2)));
        verify(newsService, times(1)).findByCriteria(anyList(), anyList());
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetNews_FatalException_ShouldReturnHttpStatusCode500() throws Exception {
        when(newsService.getById(anyLong())).thenThrow(new RuntimeException());
        mockMvc.perform(get("/news/{id}", 2L))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("INTERNAL_SERVER_ERROR")))
                .andExpect(jsonPath("$.['error code']", is(500)))
                .andExpect(jsonPath("$.message", is("Ops! Something go wrong. Try again later.")));
        verify(newsService, times(1)).getById(2L);
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testGetNews_ThrowException_ShouldReturnHttpStatus400() throws Exception {
        when(newsService.findByCriteria(anyList(), anyList())).thenThrow(SearchParameterNotFoundException.class);
        mockMvc.perform(get("/news?search=modificationDate:2020-02-25;"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)));
        verify(newsService, times(1)).findByCriteria(anyList(), anyList());
        verifyNoMoreInteractions(newsService);
    }

    @Test
    public void testCountNews_ShouldPass() throws Exception {
        when(newsService.countNews()).thenReturn(20L);
        mockMvc.perform(get("/news/count"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(20)));
        verify(newsService, times(1)).countNews();
        verifyNoMoreInteractions(newsService);
    }
}