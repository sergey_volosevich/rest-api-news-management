package com.epam.lab.controller;

import com.epam.lab.cofiguration.ControllerContextConfig;
import com.epam.lab.service.AuthorServiceImpl;
import com.epam.lab.service.NewsServiceImpl;
import com.epam.lab.service.TagServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

import static org.mockito.Mockito.mock;

@Configuration
@ComponentScan(basePackages = "com.epam.lab.controller", lazyInit = true)
@Import(ControllerContextConfig.class)
public class TestContext {

    @Bean
    @Lazy
    public NewsServiceImpl newsServiceImpl() {
        return mock(NewsServiceImpl.class);
    }

    @Bean
    @Lazy
    public AuthorServiceImpl authorServiceImpl() {
        return mock(AuthorServiceImpl.class);
    }

    @Bean
    @Lazy
    public TagServiceImpl tagServiceImpl() {
        return mock(TagServiceImpl.class);
    }
}
