package com.epam.lab.controller;

import com.epam.lab.builder.AuthorBuilder;
import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.exception.EntityNotFoundException;
import com.epam.lab.service.AuthorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class})
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AuthorControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetAuthor_AuthorFound_ShouldReturnAuthor() throws Exception {
        AuthorDTO authorDTO = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test01")
                .withSurname("Surname-test01")
                .build();
        when(authorService.getById(1L)).thenReturn(authorDTO);
        mockMvc.perform(get("/authors/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Name-test01")))
                .andExpect(jsonPath("$.surname", is("Surname-test01")));

        verify(authorService, times(1)).getById(1L);
        verifyNoMoreInteractions(authorService);
    }

    @Test
    public void testUnsupportedMethod_ShouldReturnHttpStatusCode405() throws Exception {
        mockMvc.perform(post("/authors/{id}", 1L))
                .andExpect(status().isMethodNotAllowed())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("METHOD_NOT_ALLOWED")))
                .andExpect(jsonPath("$.['error code']", is(405)))
                .andExpect(jsonPath("$.message", is("Method POST not supported. Supported " +
                        "methods: GET, PUT, DELETE")));
    }

    @Test
    public void testGetAuthor_EntityNotFound_ShouldReturnHttpStatusCode404() throws Exception {
        when(authorService.getById(1L)).thenThrow(new EntityNotFoundException());

        mockMvc.perform(get("/authors/{id}", 1L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("NOT_FOUND")))
                .andExpect(jsonPath("$.['error code']", is(404)))
                .andExpect(jsonPath("$.message", is("Author not found")));
        verify(authorService, times(1)).getById(1L);
        verifyNoMoreInteractions(authorService);
    }

    @Test
    public void testGetAuthor_InvalidPathVariable_ShouldReturnHttpStatusCode400() throws Exception {
        mockMvc.perform(get("/authors/{id}", "notNumber"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages.[0]", is("Invalid request")));
        verifyNoInteractions(authorService);
    }

    @Test
    public void testSaveAuthor_ValidRequestBody_ShouldReturnSavedAuthor() throws Exception {
        AuthorDTO inputAuthorDTO = new AuthorBuilder()
                .withName("Name-test")
                .withSurname("Surname-test")
                .build();
        AuthorDTO outputAuthorDTO = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test")
                .withSurname("Surname-test")
                .build();
        when(authorService.save(inputAuthorDTO)).thenReturn(outputAuthorDTO);

        mockMvc.perform(post("/authors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(inputAuthorDTO)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(header().string(HttpHeaders.LOCATION, "http://localhost/authors/1"))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Name-test")))
                .andExpect(jsonPath("$.surname", is("Surname-test")));

        ArgumentCaptor<AuthorDTO> captor = ArgumentCaptor.forClass(AuthorDTO.class);
        verify(authorService, times(1)).save(captor.capture());
        verifyNoMoreInteractions(authorService);

        AuthorDTO authorDTO = captor.getValue();
        assertNull(authorDTO.getId());
        assertEquals("Name-test", authorDTO.getName());
        assertEquals("Surname-test", authorDTO.getSurname());
    }

    @Test
    public void testSaveAuthor_InvalidRequestBody_ShouldReturnHttpStatus400() throws Exception {
        AuthorDTO authorDTO = new AuthorBuilder()
                .build();
        mockMvc.perform(post("/authors")
                .content(objectMapper.writeValueAsBytes(authorDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.messages", hasSize(2)))
                .andExpect(jsonPath("$.messages.[*]", containsInAnyOrder("Please provide a author name",
                        "Please provide a author surname")));

        verifyNoInteractions(authorService);
    }

    @Test
    public void testSaveAuthor_InvalidJson_ShouldReturnHttpStatus400() throws Exception {
        String json = "{\n" +
                "\t\"id\":\"not a number\",\n" +
                "\t\"name\":\"Name-test\",\n" +
                "\t\"surname\":\"Surname-test\"\n" +
                "}";
        mockMvc.perform(post("/authors")
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Malformed JSON request")));
        verifyNoInteractions(authorService);
    }


    @Test
    public void testUpdateAuthor_ValidRequestBody_ShouldReturnUpdatedAuthor() throws Exception {
        AuthorDTO inputAuthorDTO = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test")
                .withSurname("Surname-test")
                .build();

        when(authorService.update(inputAuthorDTO)).thenReturn(inputAuthorDTO);

        mockMvc.perform(put("/authors/{id}", 1L)
                .content(objectMapper.writeValueAsBytes(inputAuthorDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Name-test")))
                .andExpect(jsonPath("$.surname", is("Surname-test")));
        ArgumentCaptor<AuthorDTO> captor = ArgumentCaptor.forClass(AuthorDTO.class);
        verify(authorService, times(1)).update(captor.capture());
        verifyNoMoreInteractions(authorService);

        AuthorDTO authorDTO = captor.getValue();
        assertEquals(1L, authorDTO.getId().longValue());
        assertEquals("Name-test", authorDTO.getName());
        assertEquals("Surname-test", authorDTO.getSurname());
    }


    @Test
    public void testUpdateAuthor_InvalidRequestBody_ShouldReturnHttpStatus400() throws Exception {
        AuthorDTO inputAuthorDTO = new AuthorBuilder()
                .withId(1L)
                .withName("Nm")
                .withSurname("Su")
                .build();
        mockMvc.perform(put("/authors/{id}", 1L)
                .content(objectMapper.writeValueAsBytes(inputAuthorDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.messages", hasSize(2)))
                .andExpect(jsonPath("$.messages.[*]", containsInAnyOrder("Please provide a correct"
                        + " value for author name.", "Please provide a correct value for author surname.")));
        verifyNoInteractions(authorService);
    }

    @Test
    public void testUpdateAuthor_AuthorNotExist_ShouldReturnHttpStatus400() throws Exception {
        AuthorDTO inputAuthorDTO = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test")
                .withSurname("Surname-test")
                .build();
        when(authorService.update(inputAuthorDTO)).thenThrow(new EntityNotFoundException());
        mockMvc.perform(put("/authors/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(inputAuthorDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Failed to update author. Caused: can not find"
                        + " author with id = 1")));
        verify(authorService, times(1)).update(inputAuthorDTO);
        verifyNoMoreInteractions(authorService);
    }

    @Test
    public void testRemoveAuthor_AuthorExist_ShouldReturnHttpStatus204() throws Exception {
        long authorId = 1;
        doNothing().when(authorService).delete(authorId);

        mockMvc.perform(delete("/authors/{id}", authorId))
                .andExpect(status().isNoContent());
        verify(authorService, times(1)).delete(authorId);
        verifyNoMoreInteractions(authorService);
    }

    @Test
    public void testRemoveAuthor_AuthorNotExist_ShouldReturnHttpStatus400() throws Exception {
        long authorId = 1;
        doThrow(new EntityNotFoundException()).when(authorService).delete(authorId);
        mockMvc.perform(delete("/authors/{id}", authorId))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.['error code']", is(400)))
                .andExpect(jsonPath("$.message", is("Failed to delete author. Caused: can not find "
                        + "author with id = 1")));
        verify(authorService, times(1)).delete(authorId);
        verifyNoMoreInteractions(authorService);
    }

    @Test
    public void testGetAllAuthors_ShouldReturnFoundAuthors() throws Exception {
        AuthorDTO author1 = new AuthorBuilder()
                .withId(1L)
                .withName("Name-test")
                .withSurname("Surname-test")
                .build();
        AuthorDTO author2 = new AuthorBuilder()
                .withId(2L)
                .withName("Name-test")
                .withSurname("Surname-test")
                .build();
        List<AuthorDTO> authors = new ArrayList<>();
        authors.add(author1);
        authors.add(author2);
        when(authorService.getAll()).thenReturn(authors);
        mockMvc.perform(get("/authors"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)));
        verify(authorService, times(1)).getAll();
        verifyNoMoreInteractions(authorService);
    }
}