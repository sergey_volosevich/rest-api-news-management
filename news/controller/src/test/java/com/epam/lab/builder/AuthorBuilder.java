package com.epam.lab.builder;

import com.epam.lab.dto.AuthorDTO;

public class AuthorBuilder {

    private Long id;
    private String name;
    private String surname;

    public AuthorBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuthorBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public AuthorBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public AuthorDTO build() {
        return new AuthorDTO(id, name, surname);
    }
}
