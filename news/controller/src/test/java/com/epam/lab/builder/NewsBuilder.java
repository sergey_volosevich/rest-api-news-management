package com.epam.lab.builder;

import com.epam.lab.dto.AuthorDTO;
import com.epam.lab.dto.NewsDTO;
import com.epam.lab.dto.TagDTO;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class NewsBuilder {

    private Long id;
    private String title;
    private String shortText;
    private String fullText;
    private LocalDate creationDate;
    private LocalDate modificationDate;
    private AuthorDTO author;
    private Set<TagDTO> tags;

    public NewsBuilder() {
        this.tags = new HashSet<>();
    }

    public NewsBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public NewsBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public NewsBuilder withShortText(String shortText) {
        this.shortText = shortText;
        return this;
    }

    public NewsBuilder withFullText(String fullText) {
        this.fullText = fullText;
        return this;
    }

    public NewsBuilder withCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public NewsBuilder withModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
        return this;
    }

    public NewsBuilder withAuthor(AuthorDTO authorDTO) {
        this.author = authorDTO;
        return this;
    }

    public NewsBuilder withTag(TagDTO tagDTO) {
        this.tags.add(tagDTO);
        return this;
    }

    public NewsDTO build() {
        return new NewsDTO(id, title, shortText, fullText, creationDate, modificationDate, author, tags);
    }
}
