# Project information #

Create a backend for simple News Management application. Application is without view layer (UI).

The system should expose ***REST API*** to perform the following operations:
    
1. CRUD operations for News.

    If new tags and(or) Author are passed during creation or modification of
    a News, then they should be created in the database. (Save news message with
    author and tags should go as one step in service layer.)
    
2. CRUD operations for Tag.
3. CRUD operations for Author.

    When an author is deleted, all his news should be deleted.
    
4. Get news. All request parameters are optional and can be used in conjunction.

    Sort news by date or author. Search news by author name and surname, tag name.

5. Add tag/tags for news message;
6. Each news should have only one author;
7. Each news may have more than 1 tag;

### Tools and technologies ###

1.  **JDK version** - 8;
2.  **Database connection pooling** - HikariCP 3.4.1;
3.  **Data access** - Hibernate 5.4.12.Final;
4.  **Validation** - Hibernate Validator 6.1.0.Final;
5.  **Code coverage** - JaCoCo 0.8.4;
6.  **Build tool** - Apache Maven 3.5;
7.  **Web server** - Apache Tomcat 8.5.50;
8.  **Application container** - Spring IoC. Spring Framework 5.2.2.RELEASE;
9.  **Tool for demonstrating** - Postman v7.24.0;
10.  **Database** - PostgreSQL 11;
11.  **Testing** - JUnit4, Mockito 3.2.4, Spring test module, HyperSQL DataBase 2.5.0, Hamcrest 2.2, Jsonpath 2.4.0;
12.  **Logging** - Log4j 2.13.0.
 

### Links to test API ###

**REST API**

*   http://news-management.us-east-2.elasticbeanstalk.com/

**REACT APP**

*	http://news-management-ui.s3-website.us-east-2.amazonaws.com/

## REST API ##

### Author ###
*	**GET**: /authors - Get all authors;
*	**GET**: /authors/id - Get author by id;
*	**POST**: /authors - Create author. Author should be sent using request-body in appropriate JSON-format;

	Example of JSON-format:
	
    `{
    			"name": "Name",
    			"surname": "Surname"
    }`

*	**PUT**: /authors/id - Update concrete author. Author should be sent using request-body in appropriate JSON-format;

	Example of JSON-format:
	
    `{
                "id": "1",
    			"name": "Name",
    			"surname": "Surname"
    }`

*	**DELETE**: /authors/id - Delete author by id.


### Tag ###
*	**GET**: /tags - Get all tags;
*	**GET**: /tags/id - Get tag by id;
*	**POST**: /tags - Create tag. Tag should be sent using request-body in appropriate JSON-format;

	Example of JSON-format:

    `{
    	"name": Tag"
    }`
		
*	**PUT**: /tags/id - Update concrete tag. Tag should be sent using request-body in appropriate JSON-format;

	Example of JSON-format:

    `{
    			"id": 1,
    			"name": Tag"
    }`
    
*	**DELETE**: /tags/id - Delete tag by id.


### News ###
*	**GET**: /news?pag=limit:...;page:...; - Get part of news (pagination). Pagination is optional parameter. If you do not provide pagination, application will return all news.;
*	**GET**: /news/id - Get news by id;
*	**GET**: /news?search=...;&sort=...;&pag=limit:...;page:...; - Get news according to search and sort conditions with limit(pagination).

	***Search*** 
	
	In search parameter you can use next variables:
	
    1.  *title* - search by title of news;
    2.  *creationDate* - search news by creation date;
    3.  *modificationDate* - search news by modification date;
    4.  *authorName* - search news by author name;
    5.  *authorSurname* -search news by author surname;
    6.  *tag* - search news by tag name.

	You can use next operations for search parameters to use conditions:
	
    1.  *":"* - equals to value of parameter;
    2.  *">"* - greater than or equal to value of parameter;
    3.  *"<"* - less than or equal to value of parameter.

	Example of query to search news:
	
    1.  "/news?search=authorName:Name;" - find news by author name;
    2.  "/news?search=authorName:Name;authorSurname:Surname;" - find news by author's name and surname;
    3.  "/news?search=creationDate>2020-01-28;" - find news that was created after the date passed;
    4.  "/news?search=creationDate<2020-02-26;" - find news that were created earlier than the creation date;
    5.  "/news?search=tag:tag_test02;" - find news by tag name.
    
	Also you can combine them.

    ***Sort***
    
	In sort parameter you can use next variables:

    1.  *title* - search by title of news;
    2.  *creationDate* - search news by creation date;
    3.  *modificationDate* - search news by modification date;
    4.  *authorName* - search news by author name;
    5.  *authorSurname* -search news by author surname;
    
    The value of the sort parameter may contain the following values:

    1.  *asc* - sort news by parameter in ascending order;
    2.  *desc* - sort news by parameter in descending order.

    Example of query to sort news:

    1.  "/news?sort=creationDate:asc;" - sort news in ascending order;
    2.  "/news?sort=creationDate:desc;" - sort news in descending order;
    3.  "/news?sort=creationDate:asc;modificationDate:desc;" - sort news by creation date in ascending order, if creation dates of two news are equals,
	then sort news by modification date in descending order.

    > Sort parameter can be used only with search parameter. If you not provide search parameter, but provide sort parameter, application will return all news, without sorting. If you provide search parameter and not provide sort parameter, application will return found news with sorting news by creation date in descending order.


    ***Pag***
    
    Pag parameter is optional. If you do not provide it, application will return all found results. In pag parameter you can use next variables:
    
    1.  *limit* - how many results to return in a page;
    2.  *page* - current page;

    Example of query to get part of news:
    
    "/news?pag=limit:5;page:1;"

    ***Full query example***

    1.  "/news?search=authorName:Name-one;&sort=creationDate:asc;modificationDate:asc;&pag=limit:5;page:1;"
    2.  "/news?search=authorName:Name-one;authorSurname:Surname;&sort=creationDate:asc;modificationDate:desc;&pag=limit:5;page:1;"
<br/>

*	**POST**: /news - Create news. News should be sent using request-body in appropriate JSON-format;
	Example of JSON-format:

    `{
    			"title": "Title",
    			"shortText": "Short text",
    			"fullText": "Some text of news",
    			"creationDate": "2020-01-28",
    			"modificationDate": "2020-01-28",
    			"author": {
        				"id": 1,
        				"name": "Name",
        				"surname": "Surname"
    				  },
    			"tags": [
        			  {
            				"id": 1,
            				"name": "tag"
        			  }
    				]
    }`

	If you want to create author or tag when you create news, do not provide field id for author or tag.
    
*	**PUT**: /news/id - Update concrete news. News should be sent using request-body in appropriate JSON-format;
	Example of JSON-format:

	`{
			"id": 1,
    			"title": "Title",
    			"shortText": "Short text",
    			"fullText": "Some text of news",
    			"creationDate": "2020-01-28",
    			"modificationDate": "2020-01-28",
    			"author": {
        				"id": 1,
        				"name": "Name-test01",
        				"surname": "Surname-test01"
    				  },
    			"tags": [
        			  {
            				"id": 1,
            				"name": "Sergei"
        			  }
    				]
	}`

> If you want to assign new tag or author to news, provide other id for author or tag. If you not provide id for author or tag, application
	will be create a new author or new tag and assign them to updated news.
    
*	**DELETE**: /news/id - Delete news by id.
